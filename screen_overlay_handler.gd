extends Node

class OverlayData extends RefCounted:
	var color:Color
	var fade_in:int
	var fade_out:int
	var length:int
	var spawn_time:int
	var rect:ColorRect
	
	func _init(p_color:Color, p_fade_in:int, p_fade_out:int, p_length:int, p_rect:ColorRect) -> void:
		color = p_color
		fade_in = p_fade_in
		fade_out = p_fade_out
		length = p_length
		rect = p_rect
		spawn_time = Time.get_ticks_msec()

var overlays : Array[OverlayData] = []
var canvas_layer : CanvasLayer

func _ready() -> void:
	canvas_layer = CanvasLayer.new()
	canvas_layer.layer = 2048
	add_child(canvas_layer)
	
func set_color_overlay(in_color : Color, fade_in : int, fade_out : int, length : int) -> void:
	var new_overlay : ColorRect = ColorRect.new()
	new_overlay.z_index = RenderingServer.CANVAS_ITEM_Z_MAX
	new_overlay.top_level = true
	new_overlay.set_anchors_preset(Control.PRESET_FULL_RECT)
	new_overlay.size_flags_horizontal = Control.SIZE_EXPAND_FILL
	new_overlay.size_flags_vertical = Control.SIZE_EXPAND_FILL
	var new_overlay_data : OverlayData = OverlayData.new(in_color, fade_in, fade_out, length, new_overlay)
	new_overlay.modulate = in_color
	new_overlay.modulate.a = 0
	overlays.append(new_overlay_data)
	canvas_layer.add_child(new_overlay)

func _process( _delta:float ) -> void:
	for i in range(overlays.size()-1, -1, -1):
		var total_overlay_length := overlays[i].fade_in + overlays[i].length + overlays[i].fade_out
		#print(overlays[i].rect.modulate)
		#print(overlays[i].rect.size)
		if Time.get_ticks_msec() > overlays[i].spawn_time + total_overlay_length:
			overlays[i].rect.queue_free()
			overlays.remove_at(i)
		elif Time.get_ticks_msec() < overlays[i].spawn_time + overlays[i].fade_in:
			var fade_in_end_time := overlays[i].spawn_time + overlays[i].fade_in
			var fade_in_ratio := remap(Time.get_ticks_msec(), overlays[i].spawn_time, fade_in_end_time, 0, 1)
			overlays[i].rect.modulate.a = lerpf(0, overlays[i].color.a, fade_in_ratio)
		elif Time.get_ticks_msec() < overlays[i].spawn_time + overlays[i].fade_in + overlays[i].length:
			overlays[i].rect.modulate = overlays[i].color
		else:
			var fade_out_start_time := overlays[i].spawn_time + overlays[i].fade_in + overlays[i].length
			var fade_out_end_time := overlays[i].spawn_time + overlays[i].fade_in + overlays[i].length + overlays[i].fade_out
			var fade_out_ratio := remap(Time.get_ticks_msec(), fade_out_start_time, fade_out_end_time, 0, 1)
			overlays[i].rect.modulate.a = lerpf(overlays[i].color.a, 0, fade_out_ratio)
			

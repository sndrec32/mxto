extends MeshInstance3D

var velocity : Vector3 = Vector3.ZERO
var angleVelocity : Vector3 = Vector3.ZERO
var grawlixScale : float = 1.0
@onready var grawlix_collide = $grawlixCollide

var grawlixModels : Array[Mesh] = [
	preload("res://content/base/common/grawlixes/grawlix1.res"),
	preload("res://content/base/common/grawlixes/grawlix2.res"),
	preload("res://content/base/common/grawlixes/grawlix3.res"),
	preload("res://content/base/common/grawlixes/grawlix4.res"),
	preload("res://content/base/common/grawlixes/grawlix5.res"),
	preload("res://content/base/common/grawlixes/grawlix6.res"),
	preload("res://content/base/common/grawlixes/grawlix7.res")
]

func _ready() -> void:
	mesh = grawlixModels.pick_random()

func _process(delta):
	velocity += Vector3(0, -30 * delta, 0)
	velocity += -velocity * delta
	global_rotation = global_rotation.rotated(angleVelocity.normalized(), angleVelocity.length() * delta)
	scale = Vector3(grawlixScale, grawlixScale * 0.5, grawlixScale)
	grawlix_collide.force_shapecast_update()
	for i in grawlix_collide.get_collision_count():
		if velocity.dot( grawlix_collide.get_collision_normal(i) ) > 0:
			return
		#position = grawlix_collide.get_collision_point(i) + grawlix_collide.get_collision_normal(i) * 0.3
		velocity += grawlix_collide.get_collision_normal(i) * velocity.dot(grawlix_collide.get_collision_normal(i)) * -1.5
	position += velocity * delta

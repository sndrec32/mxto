@tool

extends ScreenSpaceParticle

func _ready() -> void:
	particleColor = Vector3(randf_range(0, 3), randf_range(0, 3), randf_range(0, 3))
	velocity = Vector3(0, 0, randf_range(-4096, -1024))
	oldPosition = pos_to_screen_space(position + Vector3(0, 0.01, 0))
	olderPosition = oldPosition
	%ParticleMesh.set_instance_shader_parameter("oldScreenPos", oldPosition)
	%ParticleMesh.set_instance_shader_parameter("currentScreenPos", pos_to_screen_space(position))
	lastUpdate = Time.get_ticks_msec()
	spawnTime = Time.get_ticks_msec()
	%ParticleMesh.get_active_material(0).set_shader_parameter("spriteTexture", particleTexture)
	if Engine.is_editor_hint():
		editorCamera = Camera3D.new()
		editorCamera.position = Vector3(0, 0, -1)
		add_child(editorCamera)

func _particle_process(delta : float) -> bool:
	position += velocity * delta
	if Time.get_ticks_msec() - spawnTime > 5000 or position.z < -10000:
		queue_free()
	return true

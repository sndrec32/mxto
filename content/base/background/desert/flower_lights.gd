@tool

extends Node3D

var lights : Array = []
var velocities : Array = []

func _ready():
	lights.resize(20)
	velocities.resize(20)
	for i in 20:
		var newSprite : Sprite3D = Sprite3D.new()
		newSprite.texture = load("res://content/base/texture/stagetex/gradientradial.png")
		newSprite.pixel_size = 0.02
		newSprite.billboard = BaseMaterial3D.BILLBOARD_ENABLED
		var newMat : ShaderMaterial = ShaderMaterial.new()
		newMat.shader = preload("res://content/base/shader/SpriteGlow.gdshader")
		newMat.set_shader_parameter("spriteTex", newSprite.texture)
		newMat.set_shader_parameter("power", 2.0)
		newSprite.material_override = newMat
		newSprite.modulate = Color(1.0, 0.5, 0.0)
		add_child(newSprite)
		lights[i] = newSprite
		velocities[i] = Vector3(randf_range(-20, 20), randf_range(-20, 20), randf_range(-20, 20))


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	for i in lights.size():
		var light = lights[i]
		velocities[i] = velocities[i] + Vector3(randf_range(-1000, 1000), randf_range(-1000, 1000), randf_range(-1000, 1000)) * delta
		velocities[i].x = clamp(velocities[i].x, -75, 75)
		velocities[i].y = clamp(velocities[i].y, -75, 75)
		velocities[i].z = clamp(velocities[i].z, -75, 75)
		light.position = light.position + velocities[i] * delta
		light.position += -light.position * delta

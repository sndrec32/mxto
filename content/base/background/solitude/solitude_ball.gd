@tool

class_name SolitudeBall extends Node3D

@onready var ball: MeshInstance3D = $Ball
@onready var plane: MeshInstance3D = $Plane

@export var height : float = 10.0
@export var period : float = 25.0
@export var size : float = 4.0

var flip_time : float = 0.0

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	ball.scale = Vector3.ONE * size
	period = randf_range(6, 48)
	size = randf_range(8, 64)
	height = randf_range(32, 256)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta: float) -> void:
	if Engine.is_editor_hint():
		ball.scale = Vector3.ONE * size
	var real_time := 0.001 * Time.get_ticks_msec()
	var comp_pos := ball.position.y
	var old_y := comp_pos
	ball.position.y = sin(real_time / period * PI) * height
	comp_pos = ball.position.y
	
	if old_y > comp_pos:
		old_y -= size * 0.5
		comp_pos -= size * 0.5
	else:
		old_y += size * 0.5
		comp_pos += size * 0.5
	
	#var shader_mat : ShaderMaterial = plane.get_active_material(0)
	if sign(old_y) != sign(comp_pos):
		flip_time = real_time
	plane.set_instance_shader_parameter("flip_time", flip_time)
	plane.set_instance_shader_parameter("real_time", real_time)
	ball.set_instance_shader_parameter("ball_position", ball.position)
	var plane_time := (real_time - flip_time)
	if plane_time <= 8.0:
		var plane_size := Vector3.ONE * size
		plane.scale = plane_size * plane_time * 0.8

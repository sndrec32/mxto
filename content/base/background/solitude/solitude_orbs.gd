extends GPUParticles3D


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	var cur_camera := get_viewport().get_camera_3d()
	global_position = cur_camera.global_position + cur_camera.global_basis.z * -64 + cur_camera.global_basis.y * 32
	DebugDraw3D.draw_sphere(global_position, 1.0, Color.RED, delta)

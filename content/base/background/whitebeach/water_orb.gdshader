shader_type spatial;

render_mode fog_disabled;

uniform vec3 glass_color : source_color;
uniform sampler2D specular_ramp : source_color, repeat_disable;
uniform sampler2D fresnel_ramp : source_color, repeat_disable;
uniform sampler2D diffuse_ramp : source_color, repeat_disable;
uniform sampler2D cross_hatch : source_color, filter_linear_mipmap_anisotropic;
uniform sampler2D screen_tex : hint_screen_texture;

instance uniform float fill_percentage;

void vertex() {
	// Called for every vertex the material is visible on.
}

float fresnel(float amount, vec3 normal, vec3 view)
{
	return pow((1.0 - clamp(dot(normalize(normal), normalize(view)), 0.0, 1.0 )), amount);
}

void fragment() {
	float cross_hatch_float = (texture(cross_hatch, UV2 * 0.2 + vec2(TIME * 0.02, TIME * -0.04)).r - 0.3) * 0.2;
	ALBEDO = glass_color;
	ALBEDO += texture(screen_tex, SCREEN_UV + vec2(cross_hatch_float, -cross_hatch_float) * 0.05).rgb * 1.0;
	ALPHA = max(0.0,pow(1.0 - pow(UV.y, 8.0), 24.0));
	if (UV.y < fill_percentage){
		discard;
	}
	if (!FRONT_FACING){
		NORMAL = vec3(0, 1, 0);
	}
}

vec2 rotate(vec2 uv, vec2 pivot, float angle)
{
	mat2 rotation = mat2(vec2(sin(angle), -cos(angle)),
						vec2(cos(angle), sin(angle)));
	uv -= pivot;
	uv = uv * rotation;
	uv += pivot;
	return uv;
}

void light() {
	float cross_hatch_float = (texture(cross_hatch, UV2 * 0.2 + vec2(TIME * 0.02, TIME * -0.04)).r - 0.5) * 0.2;
	vec3 light_mix_factor = texture(diffuse_ramp, vec2(clamp(dot(NORMAL, LIGHT) + cross_hatch_float, 0.0, 1.0) * ATTENUATION, 0.0)).rgb;
	DIFFUSE_LIGHT += light_mix_factor * LIGHT_COLOR;
	float use_fresnel = fresnel(1.0, NORMAL, VIEW);
	use_fresnel = clamp(use_fresnel + max(0.0, (1.0 - distance(fill_percentage, UV.y) * 50.0)), 0.0, 1.0);
	SPECULAR_LIGHT += texture(fresnel_ramp, vec2(use_fresnel) + cross_hatch_float, 0.0).rgb * 100.0 * ALBEDO;
	
	vec3 reflected = reflect(LIGHT, NORMAL);
	vec3 use_view = -VIEW;
	float dotprod = dot(use_view , reflected);
	float spec_add = pow(max(0.0, dotprod), 2.0);
	vec3 spec_add_color = texture(specular_ramp, vec2(spec_add + cross_hatch_float, 0.0)).rgb;
	float light_mix = clamp(dot(NORMAL, LIGHT), 0.0, 1.0) * ATTENUATION;
	SPECULAR_LIGHT += spec_add_color * light_mix * 100.0 * ALBEDO;
}

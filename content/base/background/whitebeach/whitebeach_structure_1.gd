@tool

extends Node3D

@onready var water_orb_mesh: MeshInstance3D = $WaterOrbMesh
@export var fill_curve := Curve.new()
@export var fill_period := 30.0
@export var height_curve := Curve.new()
@onready var waterfall_particle: GPUParticles3D = $GPUParticles3D
@onready var splash_particle: GPUParticles3D = $GPUParticles3D2
@onready var splash_particle_2: GPUParticles3D = $GPUParticles3D3

var offset : float = 0.0

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	offset = randf()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta: float) -> void:
	var curve_time : float = fmod(0.001 * Time.get_ticks_msec() / fill_period + offset, 1.0)
	var fill_curve_sampled := fill_curve.sample_baked(curve_time)
	water_orb_mesh.set_instance_shader_parameter("fill_percentage", fill_curve_sampled)
	
	waterfall_particle.emitting = fill_curve_sampled < 0.5
	splash_particle.emitting = fill_curve_sampled < 0.999
	splash_particle_2.emitting = fill_curve_sampled < 0.999
	var height_curve_sampled := height_curve.sample_baked(curve_time)
	water_orb_mesh.position.y = height_curve_sampled * 10.0
	waterfall_particle.position.y = height_curve_sampled * 10.0 - 256

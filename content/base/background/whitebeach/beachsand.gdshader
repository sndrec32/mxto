shader_type spatial;

render_mode skip_vertex_transform, ambient_light_disabled;

uniform sampler2D color_map;
uniform sampler2D crease_map;
uniform sampler2D noise_map;
varying vec3 world_normal;
varying vec3 world_position;
uniform vec3 ambient_color : source_color;
uniform sampler2D sand_color_ramp : source_color, repeat_disable;
uniform sampler2D rock_color_ramp : source_color, repeat_disable;
uniform sampler2D tex_blend_ramp : source_color, repeat_disable;
uniform sampler2D light_blend_ramp : source_color, repeat_disable;
uniform sampler2D fresnel_blend_ramp : source_color, repeat_disable;
uniform sampler2D rock_crosshatch : source_color, repeat_enable, filter_linear;
varying float mix_factor;

float fresnel(float amount, vec3 normal, vec3 view)
{
	return pow((1.0 - clamp(dot(normalize(normal), normalize(view)), 0.0, 1.0 )), amount);
}

void vertex() {
	world_position = (MODEL_MATRIX * vec4(VERTEX, 0.0)).xyz;
	world_normal = normalize((MODEL_MATRIX * vec4(NORMAL, 0.0)).xyz);
	VERTEX = (MODELVIEW_MATRIX * vec4(VERTEX, 1.0)).xyz;
	NORMAL = normalize((MODELVIEW_MATRIX * vec4(NORMAL, 0.0)).xyz);
}

void fragment() {
	float color_map_float = texture(color_map, UV).r;
	float crease_map_float = texture(crease_map, UV).r;
	//crease_map_float += ;
	float noise_float = texture(noise_map, UV * 64.0).r;
	mix_factor = color_map_float - 0.5;
	mix_factor += (world_normal.y - 0.5) * 0.5;
	mix_factor += -crease_map_float * 0.5;
	mix_factor += (noise_float - 0.5) * 0.5;
	mix_factor = texture(tex_blend_ramp, vec2(mix_factor, 0.0)).r;
	vec4 sand_color = texture(sand_color_ramp, vec2(crease_map_float, 0.0));
	vec2 rock_uv = vec2(crease_map_float + texture(rock_crosshatch, vec2(crease_map_float * 16.0, 0.0)).r * 0.15, 0.0);
	vec4 rock_color = texture(rock_color_ramp, rock_uv);
	ALBEDO = mix(rock_color.rgb, sand_color.rgb * 2.0, mix_factor) * crease_map_float;
	ROUGHNESS = 1.0;
}

void light() {
	vec3 light_mix_factor = texture(light_blend_ramp, vec2(clamp(dot(NORMAL, LIGHT), 0.0, 1.0) * ATTENUATION, 0.0)).rgb;
	DIFFUSE_LIGHT += light_mix_factor * LIGHT_COLOR;
	SPECULAR_LIGHT += texture(fresnel_blend_ramp, vec2(fresnel(1.0, NORMAL, VIEW), 0.0)).rgb * step(mix_factor, 0.001) * 100.0 * ALBEDO;
	vec3 reflected = reflect(LIGHT, NORMAL);
	vec3 use_view = -VIEW;
	float dotprod = dot(use_view , reflected);
	float spec_add = pow(max(0.0, dotprod), 2.0);
	vec3 spec_add_color = texture(fresnel_blend_ramp, vec2(spec_add, 0.0)).rgb;
	float light_mix = clamp(dot(NORMAL, LIGHT), 0.0, 1.0) * ATTENUATION;
	SPECULAR_LIGHT += spec_add_color * light_mix * 100.0 * ALBEDO * step(mix_factor, 0.001);
	DIFFUSE_LIGHT += ambient_color;
}

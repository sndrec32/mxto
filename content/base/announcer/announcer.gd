class_name AnnouncerPack extends Resource

@export var oob : Array[AudioStream] = []
@export var boost_power : Array[AudioStream] = []
@export var final_lap : Array[AudioStream] = []
@export var finish : Array[AudioStream] = []
@export var countdown_3 : Array[AudioStream] = []
@export var countdown_2 : Array[AudioStream] = []
@export var countdown_1 : Array[AudioStream] = []
@export var go : Array[AudioStream] = []
@export var placements : Array[AudioStream] = []

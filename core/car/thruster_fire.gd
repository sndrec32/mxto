@tool

extends ScreenSpaceParticle

func _ready() -> void:
	olderPosition = pos_to_screen_space(position)
	oldPosition = pos_to_screen_space(position + velocity * 0.01666 * 1)
	position += velocity * 0.01666 * 2
	%ParticleMesh.set_instance_shader_parameter("oldScreenPos", oldPosition)
	#position += velocity * 0.002
	%ParticleMesh.set_instance_shader_parameter("currentScreenPos", pos_to_screen_space(position))
	lastUpdate = Time.get_ticks_msec()
	spawnTime = Time.get_ticks_msec()
	%ParticleMesh.get_active_material(0).set_shader_parameter("spriteTexture", particleTexture)
	%ParticleMesh.set_instance_shader_parameter("spriteSize", particleSize)
	#var ratio : float = ((float(Time.get_ticks_msec()) * 0.001) - (float(lastUpdate) * 0.001)) / persistence
	#var particleStart = olderPosition.lerp(oldPosition, ratio)
	#var particleEnd = pos_to_screen_space(position)
	#var len = minf((Vector2(particleStart.x, particleStart.y) / abs(particleStart.z) - Vector2(particleEnd.x, particleEnd.y) / abs(particleEnd.z)).length() * abs(max(particleStart.z, particleEnd.z)), 1)
	%ParticleMesh.set_instance_shader_parameter("particleModulate", particleColor)
	if Engine.is_editor_hint():
		editorCamera = Camera3D.new()
		editorCamera.position = Vector3(0, 0, -1)
		add_child(editorCamera)

func _particle_process(delta : float) -> bool:
	#velocity += -velocity * delta * 20
	#position += velocity * delta
	particleColor = particleColor.lerp(Vector3(0, 0, 0), delta * 24)
	if Time.get_ticks_msec() - spawnTime > 128:
		queue_free()
	return true

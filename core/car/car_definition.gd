@tool

class_name CarDefinition extends Resource

@export var name : String = "My awesome new car!"
@export var ref_name : String = "CAR_REFNAME"
@export var model : PackedScene
@export var hover_height : float = 0.35
@export var cam_turn_rate : float = 24
@export var max_health : float = 100.0
@export var health_restore_rate : float = 30.0
@export var acceleration : float = 10.0
@export var top_speed : float = 5.0
@export var friction : float = 0.6
@export var drag : float = 4.0
@export var weight : float = 1400.0
@export var grip : float = 5.0
@export var angle_drag : float = 8.0
@export var recenter_factor : float = 0.03
@export var boost_topspeed_multiplier : float = 1.6
@export var dashplate_topspeed_multiplier : float = 1.7
@export var boost_and_dash_topspeed_multiplier : float = 1.9
@export var boost_accel_multiplier : float = 1.4
@export var dashplate_accel_multiplier : float = 1.5
@export var boost_and_dash_accel_multiplier : float = 1.6
@export var boost_length : float = 1.5
@export var boost_health_cost : float = 18.0
@export var turbo_add : float = 5.0
@export var turbo_add_dashplate : float = 7.0
@export var turbo_depletion : float = 6.0
@export var turbo_depletion_boost : float = 4.0
@export var turbo_depletion_dashplate : float = 3.0
@export var turbo_depletion_boost_and_dash : float = 2.0
@export var drift_accel : float = -1.0
@export var turn_accel : float = -1.0
@export var strafe_power : float = 30.0
@export var strafe_mts_mult : float = 1.5
@export var strafe_mts_laterality : float = 0.5
@export var strafe_qt_mult : float = 0.4
@export var strafe_accel : float = 30.0
@export var strafe_steer_effect : float = 2.0
@export var steer_sliding : float = 1.5
@export var steer_acceleration : float = 4.0
@export var steer_power : float = 8.0
@export var steer_power_airborne : float = 30.0
@export var steer_power_drift : float = 24.0
@export var steer_reaction_strength : float = 70.0
@export var steer_reaction_damp : float = 4.0
# 0 is vehicle nose centered, 1 is vehicle nose fully sideways
@export var redirect_power_angle : Curve = Curve.new()
@export var redirect_power_angle_drift : Curve = Curve.new()
@export var redirect_power_angle_qt : Curve = Curve.new()
@export var redirect_power_angle_mts : Curve = Curve.new()
# following curves control redirect power based on speed, and are from the range of 0 to 3000 km/h
@export var redirect_power : Curve = Curve.new()
@export var redirect_power_qt : Curve = Curve.new()
@export var redirect_power_mts : Curve = Curve.new()
@export var shift_boost_curve : Curve = Curve.new()
#@export var car_collision_radius : float = 1.0
@export var wall_colliders : Array[Vector3] = []
@export var car_colliders : Array[Capsule] = []
@export var boost_sources : Array[Vector3] = []

func calculate_accel_with_speed(in_speed : float) -> float:
	var base_max_speed := 900.0 + (weight * 0.01) + top_speed * 100.0
	var accel_ratio := remap(in_speed, 0, base_max_speed, 0, 1)
	var base_accel := lerpf(acceleration * 4, 0, accel_ratio) * MXGlobal.tick_delta
	return base_accel

func calculate_speed_after_drag(in_speed : float) -> float:
	return maxf(0.0, in_speed - MXGlobal.tick_delta * ((drag * 2.0 + in_speed * drag * 0.015) * 0.01))

func calculate_friction(in_speed : float) -> float:
	var friction_reduction_proportional := in_speed * friction * 0.05
	var friction_reduction_linear := friction * 4.0
	return friction_reduction_linear + friction_reduction_proportional

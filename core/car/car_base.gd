@tool

class_name MXRacer extends Node3D

@onready var recharge_particles := $car_visual/RechargeParticles as GPUParticles3D
@onready var car_camera := %car_camera as Camera3D 
@onready var grav_point_cast := %grav_point_cast as RayCast3D
@onready var car_visual := %car_visual as Node3D
@onready var race_hud := %race_hud as RaceHud
@onready var car_audio:= %CarAudio as AudioStreamPlayer3D

@onready var debug_car_mesh := %debug_car_mesh as MeshInstance3D
var game_car_mesh : Node3D

@export var car_definition : CarDefinition:
	set(new_definition):
		car_definition = new_definition
@onready var car_nametag := %Nametag as MeshInstance3D

var car_buffer : StreamPeerBuffer = StreamPeerBuffer.new()

var pawnID : int = 0

var stickers := preload("res://content/base/texture/emote_sticker/sticker_selection.tres")

#cached values, calculated once at the start of the tick
var current_transform := Transform3D.IDENTITY
var previous_transform := Transform3D.IDENTITY
var apparent_vel_length := 0.0
var gravity_basis := Basis.IDENTITY
var gravity_basis_prev := Basis.IDENTITY
var calced_max_health := 0.0

var apparent_vel_normalized := Vector3.ZERO # NOTE not needed? (one use)

# persistent gamestate

var state := CarState.WAITING
var boost_state := BoostState.NONE
var steer_state := SteerState.NORMAL
var last_state_change := 0

var current_position := Vector3.ZERO
var previous_position := Vector3.ZERO
var current_orientation := Quaternion.IDENTITY
var previous_orientation := Quaternion.IDENTITY
var apparent_vel := Vector3.ZERO
var previous_apparent_vel := Vector3.ZERO
var gravity_orientation := Quaternion.IDENTITY
var gravity_orientation_prev := Quaternion.IDENTITY

var health := 100.0
var max_health_boost := 0.0
var velocity := Vector3.ZERO
var knockback_vel := Vector3.ZERO
var angle_vel := Vector3.ZERO
var last_ground_position := Vector3.ZERO
var last_gravity_direction := Vector3.ZERO
var travel_direction := Vector3.FORWARD
var base_speed := 0.0
var lift_force_vel := 0.0
var lift_force := 0.0

var grounded := true
var on_booster := false
var connected_to_ground := false
var no_gravity_reset := false
var floor_type : RORStageObject.object_types = RORStageObject.object_types.NONE
var ground_time := 0
var air_time := 0
var boost_time := 0
var boostpad_time := 0
var spin_time := 0
var current_point := 0 
var current_point_real := 0
var lap := 0

var air_tilt := 0.0
var drift_dot := 0.0
var lap_progress := 0.0
var current_strafe := 0.0
var current_steering := 0.0
var current_boost_strength := 1.0
var current_turbo := 0.0
var boost_recharge := 0.0
var turn_reaction_effect := 0.0
var steer_current := 0.0
var steer_old := 0.0

# visual
var specced_player := 0
var spec_mode := 0
var spec_velocity := Vector3.ZERO
var spec_position := Vector3.ZERO
var spec_angles := Basis.IDENTITY

var cam_yaw := 0.0
var cam_pitch := 0.0
var cam_pitch_vel := 0.0
var cam_roll := 0.0

var pitstop_time := 0.0
var sticker_time := 2.0
var visual_drift_loss : float = 0.0
var damage := 0.0
var visual_rotation := Vector3.ZERO
var levelStartTime := MXGlobal.countdownTime
var level_win_time := 0
var car_height := 0.0
var camera_offset := Vector3(0.0, 1.5, 4.0)
var camera_pitch_velocity := 0.0
var camera_pitch_add := 0.0

enum CarState {
	WAITING,
	PLAY,
	FALL,
	DYING,
	DEAD,
	GOAL
}

enum BoostState {
	NONE,
	PAD,
	MANUAL,
	BOTH
}

enum SteerState {
	NORMAL,
	DRIFT,
	QUICK
}

var sound_dictionary : Dictionary = {}
var decal_container : Node3D
var thruster_container : Node3D
@onready var drift_fire_container := $drift_fire_container
@onready var nametag_background := %NametagBackground
@onready var boost_electricity := $car_visual/BoostElectricity
@onready var placement_sprite := $car_visual/PlacementSprite


var up_dir : Vector3 = Vector3.UP
var slerped_gravity_basis := Basis.IDENTITY
@onready var sticker := $car_visual/Sticker as Sprite3D

const EMOTE_ALPHA_CURVE = preload("res://content/base/common/emote_alpha_curve.tres")
const EMOTE_SCALE_CURVE = preload("res://content/base/common/emote_scale_curve.tres")
const EMOTE_Y_CURVE = preload("res://content/base/common/emote_y_curve.tres")

var spec_control_a := false
var spec_control_b := false
var spec_control_z := false

@onready var car_shadow_camera := $CarShadowViewport/CarShadowCamera as Camera3D
@onready var car_shadow_mesh := $CarShadowMesh as MeshInstance3D

var camera_basis : Basis = Basis.IDENTITY

@onready var landing_particles := $car_visual/LandingParticles as GPUParticles3D
@onready var car_shadow_viewport := $CarShadowViewport as SubViewport

var just_ticked := false
var placement_textures : Array[Texture] = [
	preload("res://content/base/texture/ui/placements/mx-1.png"),
	preload("res://content/base/texture/ui/placements/mx-2.png"),
	preload("res://content/base/texture/ui/placements/mx-3.png"),
	preload("res://content/base/texture/ui/placements/mx-4.png"),
	preload("res://content/base/texture/ui/placements/mx-5.png"),
	preload("res://content/base/texture/ui/placements/mx-6.png"),
	preload("res://content/base/texture/ui/placements/mx-7.png"),
	preload("res://content/base/texture/ui/placements/mx-8.png"),
	preload("res://content/base/texture/ui/placements/mx-9.png"),
	preload("res://content/base/texture/ui/placements/mx-10.png"),
	preload("res://content/base/texture/ui/placements/mx-11.png"),
	preload("res://content/base/texture/ui/placements/mx-12.png"),
	preload("res://content/base/texture/ui/placements/mx-13.png"),
	preload("res://content/base/texture/ui/placements/mx-14.png"),
	preload("res://content/base/texture/ui/placements/mx-15.png"),
	preload("res://content/base/texture/ui/placements/mx-16.png"),
	preload("res://content/base/texture/ui/placements/mx-17.png"),
	preload("res://content/base/texture/ui/placements/mx-18.png"),
	preload("res://content/base/texture/ui/placements/mx-19.png"),
	preload("res://content/base/texture/ui/placements/mx-20.png"),
	preload("res://content/base/texture/ui/placements/mx-21.png"),
	preload("res://content/base/texture/ui/placements/mx-22.png"),
	preload("res://content/base/texture/ui/placements/mx-23.png"),
	preload("res://content/base/texture/ui/placements/mx-24.png"),
	preload("res://content/base/texture/ui/placements/mx-25.png"),
	preload("res://content/base/texture/ui/placements/mx-26.png"),
	preload("res://content/base/texture/ui/placements/mx-27.png"),
	preload("res://content/base/texture/ui/placements/mx-28.png"),
	preload("res://content/base/texture/ui/placements/mx-29.png"),
	preload("res://content/base/texture/ui/placements/mx-30.png")]

@rpc("any_peer", "call_local", "reliable")
func request_broadcast_sticker(in_sticker_id : int) -> void:
	broadcast_sticker.rpc(in_sticker_id)

@rpc("any_peer", "call_local", "reliable")
func broadcast_sticker(in_sticker_id : int) -> void:
	sticker.texture = stickers.stickers[in_sticker_id] as Texture2D
	sticker.pixel_size = 1.5 / sticker.texture.get_size().y
	sticker_time = 0
	sticker.visible = true

func _ready() -> void:
	if Engine.is_editor_hint():
		return
	
	debug_car_mesh.queue_free()
	
	if get_parent().get_multiplayer_authority() == multiplayer.get_unique_id():
		car_camera.make_current()
		car_camera.far = 30000
		car_camera.near = 0.1
		process_priority = 100
	calced_max_health = car_definition.max_health + max_health_boost
	levelStartTime = MXGlobal.countdownTime
	lift_force = -car_definition.weight * 0.01
	if !get_parent().get_multiplayer_authority() == multiplayer.get_unique_id():
		if is_instance_valid(race_hud):
			race_hud.queue_free()
		#car_camera.queue_free()
	else:
		nametag_background.queue_free()
		placement_sprite.queue_free()
	car_audio.bus = "SFX"
	car_audio.play()
	health = calced_max_health
	
	var current_stage := MXGlobal.currentStage
	current_point = current_stage.checkpoint_respawns.size() - 1
	current_point_real = current_stage.checkpoint_respawns.size() - 1
	lap = 0
	
	var car_audio_playback := car_audio.get_stream_playback() as AudioStreamPlaybackPolyphonic
	sound_dictionary["thrust"] = car_audio_playback.play_stream(preload("res://content/base/sound/car/car_idle_thrust.wav"))
	sound_dictionary["tone"] = car_audio_playback.play_stream(preload("res://content/base/sound/car/car_idle_triangle.wav"))
	sound_dictionary["pit"] = car_audio_playback.play_stream(preload("res://content/base/sound/car/energy_restore.wav"))
	sound_dictionary["strafe"] = car_audio_playback.play_stream(preload("res://content/base/common/ball_grind.wav"))
	sound_dictionary["drift"] = car_audio_playback.play_stream(preload("res://content/base/sound/car/drift_fire.wav"))
	
	game_car_mesh = car_definition.model.instantiate()
	car_visual.add_child(game_car_mesh)
	thruster_container = Node3D.new()
	decal_container = Node3D.new()
	game_car_mesh.add_child(thruster_container)
	game_car_mesh.add_child(decal_container)
	if !get_parent().get_multiplayer_authority() == multiplayer.get_unique_id():
		var real_mesh := game_car_mesh.get_child(0) as MeshInstance3D
		real_mesh.set_layer_mask_value(20, false)
		real_mesh.set_layer_mask_value(1, true)
		car_shadow_camera.queue_free()
		car_shadow_viewport.queue_free()
		car_shadow_mesh.queue_free()
	for key:String in sound_dictionary:
		car_audio_playback.set_stream_volume(sound_dictionary[key], -60)
	for point in car_definition.boost_sources:
		var new_thruster_visual : Sprite3D = preload("res://core/car/thruster_fire.tscn").instantiate()
		new_thruster_visual.rotation_degrees = Vector3(0, 180, 0)
		for child in new_thruster_visual.get_children():
			child.process_material = child.process_material.duplicate(true)
			child.draw_pass_1 = child.draw_pass_1.duplicate(true)
			child.draw_pass_1.surface_set_material(0, child.draw_pass_1.surface_get_material(0).duplicate(true))
		thruster_container.add_child(new_thruster_visual)
		new_thruster_visual.global_position = point
	for point in car_definition.wall_colliders:
		var new_drift_fire_source := preload("res://core/car/car_fire.tscn").instantiate() as GPUParticles3D
		new_drift_fire_source.process_material = new_drift_fire_source.process_material.duplicate(true)
		var pass_mesh := new_drift_fire_source.draw_pass_1.duplicate(true) as Mesh
		pass_mesh.surface_set_material(0, pass_mesh.surface_get_material(0).duplicate(true))
		new_drift_fire_source.draw_pass_1 = pass_mesh
		new_drift_fire_source.position = point + point.normalized() * 0.1
		drift_fire_container.add_child(new_drift_fire_source)
	
	var mesh_instance:MeshInstance3D = game_car_mesh.get_child(0)
	if multiplayer and not multiplayer.get_peers().is_empty():
		mesh_instance.set_instance_shader_parameter("base_color", Net.peer_map[get_multiplayer_authority()].player_settings.base_color)
		mesh_instance.set_instance_shader_parameter("secondary_color", Net.peer_map[get_multiplayer_authority()].player_settings.secondary_color)
		mesh_instance.set_instance_shader_parameter("tertiary_color", Net.peer_map[get_multiplayer_authority()].player_settings.tertiary_color)
	else:
		mesh_instance.set_instance_shader_parameter("base_color", MXGlobal.local_settings.base_color)
		mesh_instance.set_instance_shader_parameter("secondary_color", MXGlobal.local_settings.secondary_color)
		mesh_instance.set_instance_shader_parameter("tertiary_color", MXGlobal.local_settings.tertiary_color)

func _post_stage_loaded() -> void:
	pass

func change_state(inState:CarState) -> void:
	if inState != state:
		last_state_change = MXGlobal.currentStageOverseer.localTick
		match inState:
			CarState.GOAL:
				level_win_time = MXGlobal.currentStageOverseer.localTick
				if !MXGlobal.currentlyRollback:
					if multiplayer and not multiplayer.get_peers().is_empty():
						var finisher_name:String = Net.peer_map[get_multiplayer_authority()].player_settings.username
						var badge := preload("res://content/base/ui/finish_medal.tscn").instantiate()
						MXGlobal.add_control_to_feed(badge)
						var time_elapsed := level_win_time - levelStartTime
						var time_elapsed_float := float(time_elapsed) / Engine.physics_ticks_per_second
						var seconds := floori(time_elapsed_float) % 60
						var milliseconds := floori(time_elapsed_float * 1000) % 1000
						var minutes := floori(time_elapsed_float / 60)
						badge.set_finisher_name(finisher_name, str(minutes) + ":" + str(seconds) + "." + str(milliseconds))
			CarState.DYING:
				game_car_mesh.position = Vector3.ZERO
				health = 0
				angle_vel = angle_vel.normalized()
				angle_vel *= 20
				velocity = apparent_vel * 1.5 + gravity_basis.y * MXGlobal.kmh_to_ups * 400
				current_turbo = 0
	state = inState

func set_nametag(inName : String) -> void:
	await get_tree().create_timer(0.5).timeout
	if car_nametag and is_instance_valid(car_nametag):
		var shader_mat_1 : ShaderMaterial = nametag_background.get_active_material(0).duplicate(true)
		var shader_mat_2 : ShaderMaterial = car_nametag.get_active_material(0).duplicate(true)
		nametag_background.material_override = shader_mat_1
		car_nametag.material_override = shader_mat_2
		nametag_background.mesh = nametag_background.mesh.duplicate(true)
		car_nametag.mesh = car_nametag.mesh.duplicate(true)
		car_nametag.mesh.text = inName
		var bgMesh : PlaneMesh = %NametagBackground.mesh
		bgMesh.size.x = inName.length() * 0.08

func handle_car_audio(delta : float) -> void:
	var car_audio_playback := car_audio.get_stream_playback() as AudioStreamPlaybackPolyphonic
	match state:
		CarState.PLAY:
			var vel_len : float = apparent_vel_length
			var vel_rescaled : float = clampf(pow(remap(MXGlobal.ups_to_kmh * vel_len, 0, 1600, 0, 1), 2), 0, 1)
			var thrust_volume : float = remap(vel_rescaled, 0, 1, -32, -24)
			var tone_volume : float = remap(vel_rescaled, 0, 1, -30, -5)
			if Net.connected and get_parent().get_multiplayer_authority() == multiplayer.get_unique_id():
				thrust_volume -= 10
				tone_volume -= 10
			var tone_pitch : float = remap(vel_rescaled, 0, 1, 0.4, 1.2)
			var p:ROPlayer = get_parent()
			var player_input:PlayerInput = p.get_latest_input()
			#var player_input:PlayerInput = p.get_suitable_input( true )
			var accel : bool = player_input.Accelerate == PlayerInput.PressedState.Pressed
			if !accel:
				thrust_volume = -60
			car_audio_playback.set_stream_volume(sound_dictionary["thrust"], thrust_volume)
			car_audio_playback.set_stream_volume(sound_dictionary["tone"], tone_volume)
			car_audio_playback.set_stream_pitch_scale(sound_dictionary["tone"], tone_pitch)
			
			car_audio_playback.set_stream_volume(sound_dictionary["strafe"], remap(pow(absf(current_strafe), 0.2), 0, 1, -60, -5))
			car_audio_playback.set_stream_pitch_scale(sound_dictionary["strafe"], absf(current_strafe) * 0.25 + 0.75)
			
			if floor_type == RORStageObject.object_types.PITSTOP:
				car_audio_playback.set_stream_pitch_scale(sound_dictionary["pit"], clampf((pitstop_time + 2.0) * 0.25, 0.5, 2.0))
				pitstop_time += delta
				recharge_particles.emitting = true
			else:
				car_audio_playback.set_stream_volume(sound_dictionary["pit"], -60)
				pitstop_time = lerpf(pitstop_time, 0.0, delta * 10.0)
				recharge_particles.emitting = false
			
			car_audio_playback.set_stream_volume(sound_dictionary["pit"], clampf(remap(pitstop_time, 0, 0.25, -60, 0), -60, 0))
			
			var drift_volume := minf(absf(visual_drift_loss) * 3.0 - 5, 0)
			if absf(visual_drift_loss) <= 0.0001:
				drift_volume = -60
			var drift_pitch := minf(absf(visual_drift_loss) * 2.0 + 2.0, 4.0)
			car_audio_playback.set_stream_volume(sound_dictionary["drift"], drift_volume)
			car_audio_playback.set_stream_pitch_scale(sound_dictionary["drift"], drift_pitch)
		CarState.WAITING:
			for key:String in sound_dictionary:
				car_audio_playback.set_stream_volume(sound_dictionary[key], -60)
		CarState.DYING:
			for key:String in sound_dictionary:
				car_audio_playback.set_stream_volume(sound_dictionary[key], -60)
		CarState.DEAD:
			for key:String in sound_dictionary:
				car_audio_playback.set_stream_volume(sound_dictionary[key], -60)
		CarState.FALL:
			for key:String in sound_dictionary:
				car_audio_playback.set_stream_volume(sound_dictionary[key], -60)
		CarState.GOAL:
			for key:String in sound_dictionary:
				car_audio_playback.set_stream_volume(sound_dictionary[key], -60)

func _process( delta:float ) -> void:
	delta = 0.0166666
	
	if Engine.is_editor_hint():
		car_shadow_mesh.global_position = last_ground_position + Vector3(0, -0.5, 0.0)
		for capsule in car_definition.car_colliders:
			var substeps : int = 17
			var forward : Vector3 = (capsule.p2 - capsule.p1).normalized()
			var right : Vector3 = forward.cross(Vector3.UP).normalized()
			for i in substeps:
				var t : float = (float(i) / (substeps - 1)) * PI * 2
				var line_p1 : Vector3 = (Vector3.UP * sin(t) + right * cos(t)) * capsule.radius + capsule.p1
				var line_p2 : Vector3 = (Vector3.UP * sin(t) + right * cos(t)) * capsule.radius + capsule.p2
				DebugDraw3D.draw_line(line_p1, line_p2, Color(1, 1, 1), delta)
			var tp1 : Transform3D = Transform3D(Basis(right * capsule.radius * 2, Vector3.UP * capsule.radius * 2, forward * capsule.radius * 2), capsule.p1)
			var tp2 : Transform3D = Transform3D(Basis(right * capsule.radius * 2, Vector3.UP * capsule.radius * 2, forward * capsule.radius * 2), capsule.p2)
			DebugDraw3D.draw_sphere_xf(tp1, Color(1, 1, 1), delta)
			DebugDraw3D.draw_sphere_xf(tp2, Color(1, 1, 1), delta)
		for point in car_definition.wall_colliders:
			DebugDraw3D.draw_line(point + Vector3.UP * 0.25, point + Vector3.DOWN * 0.25, Color(1, 0, 0), delta)
		for point in car_definition.boost_sources:
			DebugDraw3D.draw_arrow_line(point, point + Vector3.FORWARD, Color(1, 0, 0), 0.1, true, delta)
		return
	if !just_ticked:
		spec_control_z = spec_control_z or Input.is_action_just_pressed("SpinAttack")
		spec_control_a = spec_control_a or Input.is_action_just_pressed("Accelerate")
		spec_control_b = spec_control_b or Input.is_action_just_pressed("Boost")
		return
	game_car_mesh.position = Vector3.ZERO
	just_ticked = false
	if !is_instance_valid(MXGlobal.currentStageOverseer):
		return
	var is_own_car := true
	if multiplayer and multiplayer.has_multiplayer_peer():
		is_own_car = get_multiplayer_authority() == multiplayer.get_unique_id()
	if get_parent().place < 3 and !is_own_car:
		placement_sprite.visible = true
		placement_sprite.texture = placement_textures[get_parent().place]
	elif is_instance_valid(placement_sprite):
		placement_sprite.visible = false
	
	var p:ROPlayer = get_parent()
	var player_input:PlayerInput = p.get_latest_input()
	#var player_input:PlayerInput = p.get_suitable_input( true )volu
	var accel : bool = player_input.Accelerate == PlayerInput.PressedState.Pressed
	#var vis_rot_basis := Basis.from_euler(visual_rotation)
	car_visual.global_position = current_position
	car_visual.quaternion = current_orientation
	if sticker_time < 2.0:
		sticker.modulate.a = EMOTE_ALPHA_CURVE.sample_baked(sticker_time * 0.5)
		sticker.scale = Vector3.ONE * EMOTE_SCALE_CURVE.sample_baked(sticker_time * 0.5)
		if get_multiplayer_authority() == multiplayer.get_unique_id():
			sticker.scale *= 0.333
		sticker.position.y = EMOTE_Y_CURVE.sample_baked(sticker_time * 0.5)
		sticker_time += delta
		#if is_instance_valid(car_nametag):
			#car_nametag.visible = false
			#placement_sprite.visible = false
			#nametag_background.visible = false
	else:
		sticker.visible = false
		#if is_instance_valid(car_nametag):
			#car_nametag.visible = true
			#nametag_background.visible = true
	
	if grounded:
		var height_adjust := car_definition.hover_height * clampf(remap(lift_force, -car_definition.weight * 0.02, 0, 0.5, -0.5), -1, 1)
		game_car_mesh.position.y -= height_adjust
	else:
		game_car_mesh.position.y = lerpf(game_car_mesh.position.y, 0, delta * 12)
	
	damage += -damage * delta * 16
	
	var use_vel := velocity
	use_vel = (use_vel - gravity_basis.y * use_vel.dot(gravity_basis.y))
	use_vel = car_visual.global_transform.basis.z.lerp(use_vel, clampf(remap(use_vel.length(), 0, 5, -1, 1), 0, 1)).normalized()
	
	var use_z := current_transform.basis.z
	use_z = use_z.slide(gravity_basis.y).normalized()
	var use_y := gravity_basis.y
	var use_x := use_vel.cross(use_y).normalized()
	if !use_x.is_normalized():
		use_x = current_transform.basis.x
	
	var mesh_instance := game_car_mesh.get_child(0) as MeshInstance3D
	var health_ratio : float = minf(1.0, (health / calced_max_health) * 4.0)
	var boost_ratio : float = maxf(float(boost_time) / (car_definition.boost_length * Engine.physics_ticks_per_second), float(boostpad_time) / Engine.physics_ticks_per_second)
	var health_flash := Color(0.04, -0.01, -0.01) * (sin(0.015 * Time.get_ticks_msec()) * 0.5 + 0.5) * (1.0 - health_ratio)
	var boost_flash := Color(0, 0.03, 0.075) * (boost_ratio)
	var final_overlay := health_flash + boost_flash + Color(1, 1, 1) * damage * 0.1
	boost_electricity.get_child(0).amount_ratio = 1.0 - health_ratio
	boost_electricity.amount_ratio = (1.0 - health_ratio) + maxf(boost_ratio - 0.5, 0.0)
	final_overlay.a = 1.0
	mesh_instance.set_instance_shader_parameter("overlay_color", final_overlay * 20.0)
	boost_electricity.process_material.color = lerp(Color(1.0, 0.75, 0.5), Color(0.5, 0.75, 1.0), health_ratio)
	boost_electricity.global_basis = Basis.IDENTITY
	
	
	for child in thruster_container.get_children():
		var sprite : Sprite3D = child as Sprite3D
		var thrust_particles := sprite.get_child(0) as GPUParticles3D
		var thrust_particles_2 := sprite.get_child(1) as GPUParticles3D
		var process_mat := thrust_particles.process_material as ParticleProcessMaterial
		#var process_mat_2 := thrust_particles_2.process_material as ParticleProcessMaterial
		if accel:
			sprite.modulate = sprite.modulate.lerp(Color(1, 1, 1, 0.3), minf(1.0, delta * 32))
			if state == CarState.PLAY:
				thrust_particles.emitting = true
				process_mat.gravity = sprite.to_local(sprite.global_position + -apparent_vel * 2)
				if boost_state != BoostState.NONE:
					thrust_particles_2.emitting = true
					thrust_particles.lifetime = 0.12
					process_mat.color = lerp(process_mat.color, Color(0.8, 0.6, 0.6), minf(1.0, delta * 6))
					process_mat.scale_min = lerpf(process_mat.scale_min, 2.0 * (pow(boost_ratio, 16) * 3.0 + 1.0), minf(1.0, delta * 6))
					process_mat.scale_max = lerpf(process_mat.scale_max, 3.0 * (pow(boost_ratio, 16) * 3.0 + 1.0), minf(1.0, delta * 6))
				else:
					thrust_particles_2.emitting = false
					thrust_particles.lifetime = 0.05
					process_mat.color = lerp(process_mat.color, Color(0.5, 0.5, 0.5), minf(1.0, delta * 6))
					process_mat.scale_min = lerpf(process_mat.scale_min, 1.5, minf(1.0, delta * 6))
					process_mat.scale_max = lerpf(process_mat.scale_max, 2.0, minf(1.0, delta * 6))
			else:
				thrust_particles.emitting = false
				thrust_particles_2.emitting = false
		else:
			sprite.modulate = sprite.modulate.lerp(Color(1, 1, 1, 0), minf(1.0, delta * 32))
			thrust_particles.emitting = false
			thrust_particles_2.emitting = false
	
	if is_instance_valid(nametag_background) and get_viewport() and get_viewport().get_camera_3d():
		var shader_mat_1 := nametag_background.get_active_material(0) as ShaderMaterial
		var shader_mat_2 := car_nametag.get_active_material(0) as ShaderMaterial
		var dist:float = (nametag_background.global_position - get_viewport().get_camera_3d().position).length() * 0.2
		shader_mat_1.set_shader_parameter("plate_dist", dist)
		shader_mat_2.set_shader_parameter("plate_dist", dist)
	
	var being_specced := true
	if MXGlobal.localPlayer:
		being_specced = MXGlobal.localPlayer.controlledPawn.state == CarState.GOAL and MXGlobal.currentStageOverseer.players[MXGlobal.localPlayer.controlledPawn.specced_player] == get_parent()
	
	
	visual_rotation.x = 0
	if steer_state == SteerState.NORMAL:
		visual_rotation.y = lerpf(visual_rotation.y, steer_current * 0.2, delta * 4)
	else:
		visual_rotation.y = lerpf(visual_rotation.y, steer_current * 0.3, delta * 6)
	visual_rotation.z = lerpf(visual_rotation.z, clampf(apparent_vel_length * current_strafe * -0.005, -1, 1), delta * 16)
	
	
	car_visual.basis = car_visual.basis * Basis.from_euler(visual_rotation)
	
	
	handle_car_audio(delta)
	match state:
		CarState.GOAL:
			for child in drift_fire_container.get_children():
				child.emitting = false
			if !is_own_car: return
			if MXGlobal.currentStageOverseer.localTick - last_state_change < 180:
				car_camera.basis = car_camera.basis.orthonormalized()
				car_camera.position = car_visual.global_position + car_camera.basis.z * 5 + gravity_basis.y * 2
				car_camera.fov = lerpf(car_camera.fov, 60, delta * 8)
				return
			if specced_player < MXGlobal.currentStageOverseer.players.size():
				var specced_car := MXGlobal.currentStageOverseer.players[specced_player].controlledPawn as MXRacer
				if !is_instance_valid(specced_car) or !is_instance_valid(self) or !is_instance_valid(specced_car.car_camera) or !is_instance_valid(car_camera):
					return
				
				if MXGlobal.localPlayer and MXGlobal.localPlayer.focused:
					if spec_control_z:
						spec_control_z = false
						spec_mode += 1
						if spec_mode == 3:
							spec_mode = 0
					if spec_mode == 0 or spec_mode == 1:
						if spec_control_a:
							spec_control_a = false
							specced_player -= 1
						if spec_control_b:
							spec_control_b = false
							specced_player += 1
					if spec_mode == 1 or spec_mode == 2:
						spec_angles = spec_angles.rotated(spec_angles.x, Input.get_axis("CamForward", "CamBack") * delta * -6)
						spec_angles = spec_angles.rotated(spec_angles.y, Input.get_axis("CamLeft", "CamRight") * delta * -6)
						spec_angles = spec_angles.rotated(spec_angles.z, Input.get_axis("StrafeLeft", "StrafeRight") * delta * -4)
						spec_angles = spec_angles.orthonormalized()
					if spec_mode == 2:
						spec_velocity += car_camera.basis * Vector3(Input.get_axis("MoveLeft", "MoveRight"), 0, Input.get_axis("MoveForward", "MoveBack")) * delta * 1000
						spec_velocity += -spec_velocity * delta * 4
				
				if specced_player < 0:
					specced_player = MXGlobal.currentStageOverseer.players.size() - 1
				if specced_player >= MXGlobal.currentStageOverseer.players.size():
					specced_player = 0
				
				if spec_mode == 0: # player view
					if specced_car != self:
						var spec_vel : Vector3 = velocity
						spec_vel = (specced_car.velocity - specced_car.gravity_basis.y * specced_car.velocity.dot(specced_car.gravity_basis.y))
						spec_vel = specced_car.car_visual.global_transform.basis.z.lerp(spec_vel, min(spec_vel.length(), 5) * 0.2).normalized()
						car_camera.basis = specced_car.car_camera.basis
						#car_camera.rotation_degrees += Vector3(-15, 0, 0)
						car_camera.global_position = specced_car.car_visual.global_position + car_camera.global_basis.z * 3.0 + car_camera.global_basis.y * 1.0
						car_camera.fov = specced_car.car_camera.fov
					else:
						car_camera.basis = car_camera.basis.orthonormalized()
						car_camera.position = car_visual.global_position + car_camera.basis.z * 5 + gravity_basis.y * 2
						car_camera.fov = lerpf(car_camera.fov, 60, delta * 8)
				elif spec_mode == 1: # orbit
					var specced_gravity_basis := specced_car.gravity_basis
					specced_gravity_basis = (specced_gravity_basis * spec_angles).orthonormalized()
					car_camera.basis = specced_gravity_basis
					car_camera.position = specced_car.car_visual.global_position + car_camera.basis.z * 4
				elif spec_mode == 2: # free fly
					car_camera.basis = spec_angles
					car_camera.position += spec_velocity * delta
		CarState.WAITING:
			for child in drift_fire_container.get_children():
				child.emitting = false
			if !is_own_car and !being_specced:
				return
			var desired_cam_yaw : float = gravity_basis.z.signed_angle_to(car_visual.global_transform.basis.z, gravity_basis.y) + PI
			cam_yaw = desired_cam_yaw
			cam_pitch = deg_to_rad(-15)
			car_camera.rotation = Vector3(cam_pitch, cam_yaw, 0)
			car_camera.basis = gravity_basis * car_camera.basis
			car_camera.position = car_visual.global_transform.origin + car_camera.transform.basis.z * 3 + car_camera.transform.basis.y * 1.0
			car_camera.fov = 70
		CarState.FALL:
			for child in drift_fire_container.get_children():
				child.emitting = false
			if !is_own_car and !being_specced:
				return
		CarState.DYING:
			for child in drift_fire_container.get_children():
				child.emitting = false
			if !is_own_car and !being_specced:
				return
		CarState.PLAY:
			if !is_own_car:
				var spin_time_float : float = 1.0 - clampf((maxf(0, spin_time - Engine.physics_ticks_per_second * 2.0)) / Engine.physics_ticks_per_second, 0, 1)
				var spinning : bool = spin_time_float < 0.99
				if spinning:
					var spin_curve : Curve = preload("res://core/car/spin_curve.tres")
					game_car_mesh.rotation.y = spin_curve.sample_baked(spin_time_float) * PI * 6
				return
			
			#var checkpoint_respawns := MXGlobal.currentStage.checkpoint_respawns
			#var cur_cp := checkpoint_respawns[current_point]
			#var next_cp_index := wrapi(current_point + 1, 0, checkpoint_respawns.size())
			#var next_cp := checkpoint_respawns[next_cp_index]
			#cur_cp.debug_draw_checkpoint(MXGlobal.tick_delta)
			#next_cp.debug_draw_checkpoint(MXGlobal.tick_delta)
			var num_particles := absf(visual_drift_loss) * 5.0
			if !apparent_vel.is_zero_approx() and num_particles > 0.1:
				var i := 0
				var side_dir_vel := apparent_vel.cross(gravity_basis.y).normalized()
				var side_dir_car := (game_car_mesh.global_basis.x)
				for child in drift_fire_container.get_children():
					child.emitting = true
					num_particles = absf(visual_drift_loss) * 5.0
					var mat := child.process_material as ParticleProcessMaterial
					var fire_offset := car_definition.wall_colliders[i] + Vector3(0.2 * signf(car_definition.wall_colliders[i].x), 0.25, 0.0)
					var final_pos := game_car_mesh.global_transform.orthonormalized() * fire_offset
					
					#var side_dir_point := (final_pos - car_mesh.global_position).cross(gravity_basis.y).normalized()
					var fire_dot := signf(car_definition.wall_colliders[i].x * signf(visual_drift_loss))
					num_particles = maxf(0.0, num_particles * fire_dot)
					mat.color = Color(1.0, 0.55, 0.55) * pow(minf(num_particles * 0.05, 0.95), 0.24)
					mat.initial_velocity_min = num_particles * 1.0 + 35
					mat.initial_velocity_max = num_particles * 1.3 + 35
					child.global_position = final_pos
					child.global_basis = Basis.looking_at(apparent_vel, gravity_basis.y).rotated((side_dir_vel * 2 + side_dir_car).normalized(), PI * 0.28).scaled(Vector3(0.4, 0.4, 0.4))
					i += 1
			else:
				for child in drift_fire_container.get_children():
						child.emitting = false
			slerped_gravity_basis = slerped_gravity_basis.slerp(gravity_basis, delta * 8).orthonormalized()
			var target_fov := remap(base_speed * MXGlobal.ups_to_kmh * 60, 0, 1600, 60, 80)
			target_fov += remap(current_turbo, 0, car_definition.turbo_add, 0, 20)
			car_camera.fov = lerpf(car_camera.fov, minf(100, target_fov), MXGlobal.tick_delta * 2)
			var visual_random_vel := maxf(0.0, apparent_vel_length - MXGlobal.kmh_to_ups * 1000) * (pow(boost_ratio, 8) * 12.0 + 1.0)
			var rand1 := (randf() - 0.5) * visual_random_vel * 0.001
			var rand2 := (randf() - 0.5) * visual_random_vel * 0.001
			var rand3 := (randf() - 0.5) * visual_random_vel * 0.001
			game_car_mesh.rotation = Vector3(rand1, rand2, rand3)
			
			if grounded:
				var camera_basis_z := travel_direction.slide(slerped_gravity_basis.y).normalized()
				if apparent_vel_length < 10.0:
					camera_basis_z = current_transform.basis.z
				var camera_basis_x := camera_basis_z.cross(slerped_gravity_basis.y).normalized()
				var camera_basis_y := camera_basis_z.cross(camera_basis_x).normalized()
				var target_camera_basis := Basis(camera_basis_x, -camera_basis_y, -camera_basis_z).orthonormalized()
				var vehicle_angle_offset := Basis(Quaternion(current_transform.basis.y, gravity_basis.y))
				target_camera_basis = vehicle_angle_offset * target_camera_basis
				camera_basis = camera_basis.slerp(target_camera_basis, delta * car_definition.cam_turn_rate).orthonormalized()
			else:
				var target_camera_basis := current_transform.basis
				target_camera_basis = target_camera_basis.rotated(current_transform.basis.y, deg_to_rad(180))
				cam_roll = lerpf(cam_roll, air_tilt * steer_current * -0.5, delta * 10)
				target_camera_basis = target_camera_basis.rotated(current_transform.basis.y, -cam_roll)
				target_camera_basis = target_camera_basis.rotated(current_transform.basis.z, cam_roll)
				slerped_gravity_basis = target_camera_basis * Basis.from_euler(Vector3(deg_to_rad(-20), 0, 0))
				camera_basis = camera_basis.slerp(target_camera_basis, delta * 8).orthonormalized()
			
			var vehicle_angle_offset_2 := Quaternion(current_transform.basis.z, travel_direction).get_angle()
			cam_pitch_vel -= vehicle_angle_offset_2 * delta * 2.0
			cam_pitch = clamp(cam_pitch + cam_pitch_vel * delta, -20, 20)
			cam_pitch_vel += -cam_pitch * delta * 24
			cam_pitch_vel += -cam_pitch_vel * delta * 6
			
			print(cam_pitch)
			
			var use_basis := camera_basis
			use_basis *= Basis.from_euler(Vector3(-cam_pitch, 0, 0))
			
			if Input.is_action_pressed("RearView"):
				use_basis = use_basis.rotated(current_transform.basis.y, PI)
			car_camera.position = car_visual.global_position + use_basis.z * 3.0 + use_basis.y * 1.5
			car_camera.basis = use_basis * Basis.from_euler(Vector3(deg_to_rad(-16), 0, 0))
			
			var spin_time_float : float = 1.0 - clampf((maxf(0, spin_time - Engine.physics_ticks_per_second * 2.0)) / Engine.physics_ticks_per_second, 0, 1)
			var spinning : bool = spin_time_float < 0.99
			if spinning:
				var spin_curve : Curve = preload("res://core/car/spin_curve.tres")
				game_car_mesh.rotation.y = spin_curve.sample_baked(spin_time_float) * PI * 6
			
	var desired_car_height := 100.0
	if grounded:
		for point in car_definition.wall_colliders:
			var use_point := point - Vector3(0, 0.1, 0)
			var global_point : Vector3 = game_car_mesh.global_transform.orthonormalized() * use_point
			var depth := (global_point - last_ground_position).dot(last_gravity_direction)
			if is_own_car and depth < 0.06:
				var spark_chance := clampf(remap((depth - 0.06) * apparent_vel_length * 0.33, 0.0, -5.0, 0.00, 1.0), 0.00, 1.0)
				if randf() < spark_chance:
					var spark := preload("res://content/base/effects/particles/BallSpark.tscn").instantiate()
					spark.position = global_point
					spark.velocity = apparent_vel * randf_range(0.9, 1.05) + gravity_basis.y * apparent_vel_length * 0.05
					spark.velocity += gravity_basis.y * apparent_vel_length * randf_range(0.0, 0.2)
					#spark.velocity += altered_normal * apparent_vel_length * randf_range(0.2, 0.5)
					spark.velocity += Vector3(randf_range(apparent_vel_length * -0.15, apparent_vel_length * 0.15), randf_range(apparent_vel_length * -0.15, apparent_vel_length * 0.15), randf_range(apparent_vel_length * -0.15, apparent_vel_length * 0.15))
					MXGlobal.add_child(spark)
				desired_car_height = minf(desired_car_height, depth)
	#else:
		#desired_car_height = -1.0
	car_height = desired_car_height
	#car_height = lerpf(car_height, desired_car_height, minf(1.0, delta * 24))
	var final_car_height := (maxf(0.0, -car_height) - 0.0)
	#print(final_car_height)
	game_car_mesh.global_position += gravity_basis.y * final_car_height
	
	if is_own_car:
		var shbz := gravity_basis.y
		var shbx := gravity_basis.y.cross(car_visual.basis.z).normalized()
		var shby := shbz.cross(shbx).normalized()
		car_shadow_camera.global_position = game_car_mesh.global_position + gravity_basis.y * 4.0
		car_shadow_camera.global_basis = Basis(shbx, shby, shbz);
		car_shadow_mesh.global_position = last_ground_position
		car_shadow_mesh.global_basis = Basis(shbx, shby, shbz);
		var shadow_shader_mat := car_shadow_mesh.get_active_material(0) as ShaderMaterial
		shadow_shader_mat.set_shader_parameter("car_shadow", car_shadow_viewport.get_texture())

func do_car_driving( inputs:PlayerInput = PlayerInput.Neutral ) -> void:
	# independent stuff, doesnt rely on anything else
	gravity_orientation_prev = gravity_orientation
	var boost_to_recharge := minf(boost_recharge, MXGlobal.tick_delta * 60)
	boost_recharge -= boost_to_recharge
	health = minf(calced_max_health, health + boost_to_recharge)
	boost_time = maxi(boost_time - 1, 0)
	boostpad_time = maxi(boostpad_time - 1, 0)
	
	var kmh := remap(apparent_vel_length * MXGlobal.ups_to_kmh, 0, 3000, 0, 1)
	# inputs
	var in_steer_x := inputs.SideMoveAxis
	var in_strafe_left := minf(1.0, inputs.StrafeLeft * 1.25)
	var in_strafe_right := minf(1.0, inputs.StrafeRight * 1.25)
	var in_accel := inputs.Accelerate == PlayerInput.PressedState.Pressed
	
	#var steer_accel_mult := 1.0
	
	in_steer_x = pow(absf(in_steer_x), 2) * signf(in_steer_x)
	steer_old = steer_current
	steer_current = in_steer_x
	
	var lift_force_strafe_proportion := minf(1.0, remap(lift_force, -car_definition.weight * 0.02, 0, 0.2, 2))
	
	var strafe_accel_mult := 1.0
	if floor_type == RORStageObject.object_types.PITSTOP:
		health = minf(calced_max_health, health + car_definition.health_restore_rate * MXGlobal.tick_delta)
	elif floor_type == RORStageObject.object_types.DIRT and boost_state == BoostState.NONE:
		base_speed += -base_speed * MXGlobal.tick_delta * 0.5
	elif floor_type == RORStageObject.object_types.ICE:
		#steer_accel_mult = 0.2
		strafe_accel_mult *= 0.1
	strafe_accel_mult *= lift_force_strafe_proportion
	#print(lift_force_strafe_proportion)
	#var steer_accel_true := lerpf(car_definition.steer_accel, car_definition.steer_accel * 1.5, absf(in_steer_x))
	current_strafe = move_toward( current_strafe, in_strafe_left - in_strafe_right, car_definition.strafe_accel * MXGlobal.tick_delta * strafe_accel_mult )
	
	if spin_time > 0:
		spin_time -= 1
	elif (inputs.SpinAttack == PlayerInput.PressedState.JustPressed):
		spin_time = int(Engine.physics_ticks_per_second * 3.0)
		boost_time = 0
		boostpad_time = 0
		boost_state = BoostState.NONE
		if !MXGlobal.currentlyRollback:
			MXGlobal.play_sound_from_node(preload("res://content/base/sound/car/car_spin.wav"), game_car_mesh)
	
	#var spin_time_float : float = 1.0 - clampf((maxf(0, spin_time - Engine.physics_ticks_per_second * 2.0)) / Engine.physics_ticks_per_second, 0, 1)
	#var spinning : bool = spin_time_float < 0.99
	
	if (inputs.Boost == PlayerInput.PressedState.JustPressed) and lap > 1:
		if health > 1.0:
			if boost_state == BoostState.NONE:
				boost_time = floori(car_definition.boost_length * Engine.physics_ticks_per_second)
				boost_state = BoostState.MANUAL
				current_turbo += car_definition.turbo_add
				if !MXGlobal.currentlyRollback:
					var car_audio_playback := car_audio.get_stream_playback() as AudioStreamPlaybackPolyphonic
					#var boost_sound := car_audio_playback.play_stream(preload("res://content/base/sound/car/manual_boost_default.wav"), 0, -5.0, 1.0)
					car_audio_playback.play_stream(preload("res://content/base/sound/car/manual_boost_default.wav"), 0, -5.0, 1.0)
					var new_boost_effect := preload("res://core/car/boost_effect.tscn").instantiate()
					new_boost_effect.rotation_degrees = Vector3(0, 180, 0)
					new_boost_effect.life_time = floor(car_definition.boost_length * 1000) + 2000
					car_visual.add_child(new_boost_effect)
					new_boost_effect.global_position = car_visual.global_position
		else:
			if boost_time == 0:
				if !MXGlobal.currentlyRollback:
					var car_audio_playback := car_audio.get_stream_playback() as AudioStreamPlaybackPolyphonic
					car_audio_playback.play_stream(preload("res://content/base/sound/car/dry_boost.wav"), 0, -5.0, 1.0)
				boost_time = floori(car_definition.boost_length * Engine.physics_ticks_per_second)
				current_turbo = maxf(current_turbo + car_definition.turbo_add * 0.1, car_definition.turbo_add)
	
	grav_point_cast.position = previous_position
	grav_point_cast.target_position = grav_point_cast.to_local(current_position)
	grav_point_cast.collision_mask = 4
	# NOTE 3-4us, not optimizable unless removed
	grav_point_cast.force_raycast_update()
	
	# NOTE doesn't really matter how slow this is since it's infrequent
	if grav_point_cast.is_colliding():
		if grav_point_cast.get_collider().get_parent() is BoostPad:
			if inputs.Accelerate == PlayerInput.PressedState.JustPressed:
				base_speed += 0.1
			if !on_booster:
				var active_booster : BoostPad = grav_point_cast.get_collider().get_parent()
				active_booster.execute_boost()
				boostpad_time = Engine.physics_ticks_per_second
				var turbo_add := car_definition.turbo_add_dashplate * 1.0 * active_booster.current_boost_intensity
				current_turbo += turbo_add
				boost_recharge += (active_booster.current_boost_intensity - 1.0) * 15
				var car_audio_playback := car_audio.get_stream_playback() as AudioStreamPlaybackPolyphonic
				if !MXGlobal.currentlyRollback:
					var new_boost_effect := preload("res://core/car/boost_effect.tscn").instantiate()
					new_boost_effect.rotation_degrees = Vector3(0, 180, 0)
					new_boost_effect.life_time = 3000
					game_car_mesh.add_child(new_boost_effect)
					new_boost_effect.global_position = car_visual.global_position
					new_boost_effect.global_basis = new_boost_effect.global_basis.orthonormalized()
					car_audio_playback.play_stream(preload("res://content/base/common/boost_pad.wav"), 0, -5.0, 1.0)
				if boost_state == BoostState.NONE:
					boost_state = BoostState.PAD
				else: if boost_state == BoostState.MANUAL:
					boost_state = BoostState.BOTH
				on_booster = true
		elif grav_point_cast.get_collider().get_parent() is JumpPlate and grounded:
			grounded = false
			ground_time = 0
			velocity = (gravity_basis.y * apparent_vel_length + previous_apparent_vel) * 0.8
	else:
		on_booster = false
	grav_point_cast.collision_mask = 1
	
	# NOTE boost section ~1.65us -> 1.35us by removing the allocation
	if boost_state == BoostState.MANUAL:
		health = maxf(1, health - car_definition.boost_health_cost * (1.0 / car_definition.boost_length) * MXGlobal.tick_delta)
		if boost_time == 0 or health <= 1:
			boost_state = BoostState.NONE
	elif boost_state == BoostState.PAD:
		if boostpad_time == 0:
			boost_state = BoostState.NONE
	elif boost_state == BoostState.BOTH:
		health = maxf(1, health - car_definition.boost_health_cost * (1.0 / car_definition.boost_length) * MXGlobal.tick_delta)
		if (boost_time == 0 or health <= 1) and boostpad_time == 0:
			boost_state = BoostState.NONE
		elif boost_time == 0 or health <= 1:
			boost_state = BoostState.PAD
		elif boostpad_time == 0:
			boost_state = BoostState.MANUAL
	
	if boost_state == BoostState.NONE:
		current_turbo = maxf(0.0, current_turbo - car_definition.turbo_depletion * MXGlobal.tick_delta)
	elif boost_state == BoostState.MANUAL:
		current_turbo = maxf(0.0, current_turbo - car_definition.turbo_depletion_boost * MXGlobal.tick_delta)
	elif boost_state == BoostState.PAD:
		current_turbo = maxf(0.0, current_turbo - car_definition.turbo_depletion_dashplate * MXGlobal.tick_delta)
	elif boost_state == BoostState.BOTH:
		current_turbo = maxf(0.0, current_turbo - car_definition.turbo_depletion_boost_and_dash * MXGlobal.tick_delta)
	
	current_turbo += -current_turbo * MXGlobal.tick_delta * 0.1
	
	if grounded:
		#var x := current_transform * PackedVector3Array(car_definition.wall_colliders)
		for point in car_definition.wall_colliders:
			# NOTE truly disgusting ~3.0us -> ~2.4us so maybe not worth being unreadable
			var depth := (((current_transform * point) - current_position).dot(gravity_basis.y) \
				if gravity_basis.y.dot(current_transform.basis.y) >= 0 \
				else ((current_transform * point) - current_position).dot(gravity_basis.y) * -1)
			angle_vel += ((current_transform * point) - current_position).cross(gravity_basis.y) * depth * -(16.0 if depth <= 0 else 8.0)
			# old version
			#var global_point : Vector3 = current_transform * point
			#var about_origin := global_point - current_position
			#var depth := (global_point - current_position).dot(gravity_basis.y)
			#if gravity_basis.y.dot(current_transform.basis.y) < 0:
				#depth *= -1.0
			#var reorient_strength := 16.0
			#if depth > 0:
				#reorient_strength = 8.0
			#angle_vel += about_origin.cross(gravity_basis.y) * depth * -reorient_strength
	else:
		air_time += 1
	#angle_vel += current_transform.basis.y * current_steering * MXGlobal.tick_delta * car_definition.steer_power * 2.5
	#angle_vel += -angle_vel * car_definition.grip * MXGlobal.tick_delta
	
	if grounded and inputs.Accelerate == PlayerInput.PressedState.JustPressed and steer_state != SteerState.NORMAL:
		steer_state = SteerState.NORMAL
	
	var use_z := current_transform.basis.z.slide(gravity_basis.y).normalized()
	var use_x := current_transform.basis.x.slide(gravity_basis.y).normalized()
	
	var angle_from_vel_dir := (travel_direction.slide(gravity_basis.y).normalized().signed_angle_to(use_z, gravity_basis.y) / PI) * 2.0
	
	if !grounded:
		visual_drift_loss = 0
		steer_state = SteerState.NORMAL
		lift_force = 0
		lift_force_vel = 0
	else:
		var drift_magic := apparent_vel_length * (absf(current_steering) * 2.0 + absf(angle_from_vel_dir * 1000))
		visual_drift_loss = maxf(0.0, (drift_magic - 4000 * car_definition.grip)) * -0.000025 * signf(angle_from_vel_dir)
		if drift_magic >= 1000 * car_definition.grip or (in_strafe_left > 0.05 and in_strafe_right > 0.05):
			steer_state = SteerState.DRIFT
		else:
			if signf(current_strafe) != signf(current_steering) or absf(current_steering) < 1.0:
				steer_state = SteerState.NORMAL
		if steer_state == SteerState.DRIFT and (car_definition.drift_accel < 0 or in_accel):
			base_speed += drift_magic * car_definition.drift_accel * MXGlobal.tick_delta * 0.000001
	
	# // HANDLE STRAFING // #
	var strafe_add := use_x * current_strafe * (maxf(base_speed * 60, apparent_vel_length) if grounded else base_speed * 60) * car_definition.strafe_power * 0.01
	var strafe_steer_parity := current_strafe * signf(angle_from_vel_dir)
	if steer_state == SteerState.NORMAL and grounded:
		strafe_add += use_x * current_steering * apparent_vel_length * car_definition.steer_sliding * 0.01
	elif steer_state == SteerState.DRIFT and !is_zero_approx(current_strafe):
		if strafe_steer_parity > 0:
			strafe_add *= lerpf(1.0, car_definition.strafe_qt_mult, absf(strafe_steer_parity))
		else:
			strafe_add *= lerpf(1.0, car_definition.strafe_mts_mult, absf(strafe_steer_parity))
			strafe_add = strafe_add.lerp(strafe_add.project(travel_direction), car_definition.strafe_mts_laterality)
		
	# // HANDLE AIR MOVEMENT // #
	
	#var old_air_tilt := air_tilt
	if !grounded:
		#var old_desired_use_z := use_z.rotated(use_x, air_tilt).slide(current_transform.basis.x).normalized()
		air_tilt = clampf(air_tilt + (inputs.ForwardMoveAxis + absf(in_steer_x * 0.3)) * MXGlobal.tick_delta * 4, -1.0, 1.0)
		var desired_use_z := use_z.rotated(use_x, air_tilt).slide(current_transform.basis.x).normalized()
		#var use_z_diff := Quaternion(use_z, old_desired_use_z)
		#var rotate_factor := maxf(0.0, (air_tilt - old_air_tilt) * 0.5)
		#velocity = velocity.rotated(use_x, rotate_factor)
		if !connected_to_ground:# and calced_current_checkpoint.reset_gravity:
			var to_gravity := Quaternion(gravity_basis.y, Vector3.UP)
			if to_gravity.is_normalized() and to_gravity.get_axis().is_normalized():
				var gravity_orientation_change := Quaternion(to_gravity.get_axis(), to_gravity.get_angle() * MXGlobal.tick_delta * 8)
				gravity_orientation = gravity_orientation_change * gravity_orientation
				current_orientation = gravity_orientation_change * current_orientation
				var new_basis := Basis(current_orientation)
				air_tilt = clampf(air_tilt - current_transform.basis.z.signed_angle_to(new_basis.z, new_basis.x) * 0.5, -1.0, 1.0)
		current_orientation = Quaternion(current_transform.basis.z, desired_use_z) * current_orientation
		angle_vel += -angle_vel * MXGlobal.tick_delta * 4
	
	# // HANDLE STEERING CONTROLS // #
	
	var use_target := car_definition.steer_power
	if steer_state == SteerState.DRIFT:
		use_target = car_definition.steer_power_drift
	if !grounded:
		use_target = car_definition.steer_power_airborne
	use_target += strafe_steer_parity * car_definition.strafe_steer_effect
	current_steering = lerpf(current_steering, use_target * in_steer_x, car_definition.steer_acceleration * MXGlobal.tick_delta * 10)
	
	turn_reaction_effect += (steer_current - steer_old) * car_definition.steer_reaction_strength * 3.5
	turn_reaction_effect += -turn_reaction_effect * MXGlobal.tick_delta * 11 * car_definition.steer_reaction_damp
	
	var total_turning := (current_steering + turn_reaction_effect) / (1.0 + car_definition.weight * 0.002)
	
	if grounded:
		angle_vel += gravity_basis.y * total_turning * MXGlobal.tick_delta * 5.0
	else:
		total_turning = current_steering / (1.0 + car_definition.weight * 0.002)
		current_orientation = Quaternion(gravity_basis.y, total_turning * MXGlobal.tick_delta) * current_orientation
	base_speed += absf(total_turning) * car_definition.turn_accel * MXGlobal.tick_delta * 0.01
	
	
	# // HANDLE TRAVEL DIRECTION // #
	
	if grounded:
		# -1 to 1
		angle_vel += -angle_vel * car_definition.angle_drag * MXGlobal.tick_delta
		var angle_sample := absf((travel_direction.angle_to(use_z) / PI) * 2.0)
		base_speed = maxf(0.0, base_speed - maxf(0.0, angle_sample - 1.0) * MXGlobal.tick_delta * 12)
		var use_angle_mod:float
		if steer_state == SteerState.DRIFT:
			use_angle_mod = car_definition.redirect_power_angle_drift.sample_baked(angle_sample)
		else:
			use_angle_mod = car_definition.redirect_power_angle.sample_baked(angle_sample)
		var use_speed_mod := car_definition.redirect_power.sample_baked(absf(kmh))
		if strafe_steer_parity > 0:
			use_angle_mod = lerpf(use_angle_mod, car_definition.redirect_power_angle_qt.sample_baked(angle_sample), absf(current_strafe))
			use_speed_mod = lerpf(use_speed_mod, car_definition.redirect_power_qt.sample_baked(absf(kmh)), absf(current_strafe))
		elif strafe_steer_parity < 0:
			use_angle_mod = lerpf(use_angle_mod, car_definition.redirect_power_angle_mts.sample_baked(angle_sample), absf(current_strafe))
			use_speed_mod = lerpf(use_speed_mod, car_definition.redirect_power_mts.sample_baked(absf(kmh)), absf(current_strafe))
		var lift_force_steer_proportion := minf(1.0, remap(lift_force, -car_definition.weight * 0.02, 0, 0.8, 2))
		use_angle_mod *= lift_force_steer_proportion
		travel_direction = travel_direction.slerp(use_z, minf(1.0, ((5.0 / maxf(0.01, travel_direction.angle_to(use_z))) * use_angle_mod * use_speed_mod) * MXGlobal.tick_delta * clampf(remap(lift_force, -car_definition.weight * 0.02, car_definition.weight * 0.01, 1, 0.25), 0.25, 1))).normalized()
	else:
		travel_direction = travel_direction.slerp(use_z, MXGlobal.tick_delta * 30).normalized()
		velocity = velocity.rotated(gravity_basis.y, velocity.slide(gravity_basis.y).normalized().signed_angle_to(use_z, gravity_basis.y) * 0.2)
	
	
	# // HANDLE VEHICLE ORIENTATION AND ANGULAR VELOCITY // #
	
	previous_position = current_position
	previous_orientation = current_orientation
	if !angle_vel.is_zero_approx():
		current_orientation = Quaternion(angle_vel.normalized(), angle_vel.length() * MXGlobal.tick_delta) * current_orientation
	current_orientation = current_orientation.normalized()
	
	# // HANDLE BOOST STATES // #
	
	var base_max_speed := 900.0 + (car_definition.weight * 0.01) + car_definition.top_speed * 100.0 + current_turbo * 50.0
	if boost_state == BoostState.PAD:
		base_max_speed = base_max_speed * car_definition.dashplate_topspeed_multiplier
	elif boost_state == BoostState.MANUAL:
		base_max_speed = base_max_speed * car_definition.boost_topspeed_multiplier
	elif boost_state == BoostState.BOTH:
		base_max_speed = base_max_speed * car_definition.boost_and_dash_topspeed_multiplier
	var accel_ratio := remap(apparent_vel_length * MXGlobal.ups_to_kmh, 0, base_max_speed, 0, 1)
	var base_accel := lerpf(car_definition.acceleration * 4, 0, accel_ratio) * MXGlobal.tick_delta * float(in_accel)
	if !grounded:
		base_accel = car_definition.acceleration * MXGlobal.tick_delta * float(in_accel)
	if boost_state == BoostState.PAD:
		base_accel = base_accel * car_definition.dashplate_accel_multiplier
	elif boost_state == BoostState.MANUAL:
		base_accel = base_accel * car_definition.boost_accel_multiplier
	elif boost_state == BoostState.BOTH:
		base_accel = base_accel * car_definition.boost_and_dash_accel_multiplier
	
	var just_landed_vel := Vector3.ZERO
	if grounded:
		#tt = Time.get_ticks_usec()
		base_speed = maxf(0.0, base_speed - MXGlobal.tick_delta * ((car_definition.drag * 2.0 + apparent_vel_length * car_definition.drag * 0.015) * 0.01))
		#base_speed = move_toward(base_speed, 0, use_drag * 2 * MXGlobal.tick_delta)
		var accel_add := base_accel * MXGlobal.tick_delta
		if accel_add > 0:
			base_speed += accel_add
	else:
		var old_vel := velocity
		#var flat_vel := velocity.dot(gravity_basis.y) * gravity_basis.y
		velocity += gravity_basis.y * -80 * MXGlobal.tick_delta
		var air_redirect := gravity_basis.y * maxf(0.0, apparent_vel.dot(gravity_basis.y)) * MXGlobal.tick_delta * 1.5
		air_redirect += current_transform.basis.y * apparent_vel.dot(current_transform.basis.y) * MXGlobal.tick_delta * 1.0
		if velocity.dot(gravity_basis.y) < 0:
			base_accel *= remap(air_tilt, 0, 1, 1, 3)
			#air_redirect += current_transform.basis.y * apparent_vel.dot(current_transform.basis.y) * MXGlobal.tick_delta * 3.0
			#var use_tilt := clampf((air_tilt * 2.0 - 1.0), -1, 1)
			#air_redirect += -flat_vel.normalized() * use_tilt * 80 * MXGlobal.tick_delta
		velocity += -air_redirect
		velocity = velocity.normalized() * old_vel.length()
		velocity += current_transform.basis.z * base_accel
		velocity += gravity_basis.y * minf(0.0,-80 + apparent_vel_length) * MXGlobal.tick_delta
	
	if !velocity.is_zero_approx():
		velocity = Quaternion(gravity_basis_prev.y, gravity_basis.y.lerp(gravity_basis_prev.y, 0.35).normalized()) * velocity
	
	var neither_hit := true
	var collided := false
	var cast_from := previous_position
	var total_movement := knockback_vel + strafe_add + velocity
	if grounded:
		ground_time += 1
		travel_direction = travel_direction.slide(gravity_basis.y).normalized()
		total_movement += travel_direction * base_speed * 60
	else:
		ground_time = 0
		#total_movement += travel_direction * base_speed * 30
	
	# CONTINUOUS COLLISION SECTION #
	
	var total_move_len := total_movement.length() * MXGlobal.tick_delta
	var desired_position := current_position + total_movement * MXGlobal.tick_delta
	var movement_remainder := 1.0
	for i in 4:
		grav_point_cast.position = cast_from
		grav_point_cast.target_position = grav_point_cast.to_local( desired_position )
		grav_point_cast.force_update_transform()
		grav_point_cast.force_raycast_update()
		if grav_point_cast.is_colliding():
			neither_hit = false
			if !grounded:
				just_landed_vel = velocity
			grounded = true
			collided = true
			connected_to_ground = true
			var other := grav_point_cast.get_collider() as RORStageObject
			var collision_point := grav_point_cast.get_collision_point()
			var data := other.road_info[grav_point_cast.get_collision_face_index()]
			var bary_coords: Vector3 = Geometry3D.get_triangle_barycentric_coords(collision_point, data[0], data[1], data[2])
			var up_normal: Vector3 = ((data[3] * bary_coords.x) + (data[4] * bary_coords.y) + (data[5] * bary_coords.z)).normalized()
			gravity_orientation = Quaternion(gravity_basis.y, up_normal) * gravity_orientation
			floor_type = other.object_type
			last_ground_position = collision_point
			last_gravity_direction = up_normal
			movement_remainder *= (1.0 - ((collision_point - cast_from).length() / total_move_len))
			travel_direction = (travel_direction - travel_direction.dot(up_normal) * up_normal).normalized()
			total_movement = (travel_direction * base_speed * 60 + knockback_vel + strafe_add + velocity) * movement_remainder
			current_position = collision_point + up_normal * car_definition.hover_height
			cast_from = current_position
			desired_position = current_position + total_movement * MXGlobal.tick_delta
			velocity = Vector3.ZERO
		if !collided:
			break
	#Debug.record( "movement i guess", Time.get_ticks_usec() - tt )
	
	# COLLISION CHECK UNDER CAR #
	
	if !collided:
		#tt = Time.get_ticks_usec()
		grav_point_cast.position = desired_position + gravity_basis.y
		grav_point_cast.target_position = grav_point_cast.to_local( desired_position + -gravity_basis.y * car_definition.hover_height * 100.0 )
		grav_point_cast.force_update_transform()
		grav_point_cast.force_raycast_update()
		if grav_point_cast.is_colliding():
			var other := grav_point_cast.get_collider() as RORStageObject
			var collision_point := grav_point_cast.get_collision_point()
			last_ground_position = collision_point
			var data := other.road_info[grav_point_cast.get_collision_face_index()]
			var bary_coords: Vector3 = Geometry3D.get_triangle_barycentric_coords(collision_point, data[0], data[1], data[2])
			var up_normal: Vector3 = (data[3] * bary_coords.x) + (data[4] * bary_coords.y) + (data[5] * bary_coords.z)
			neither_hit = false
			var to_gravity := Quaternion(gravity_basis.y, up_normal)
			if (desired_position - collision_point).length() < car_definition.hover_height * (2.0 if grounded else 1.0):
				collided = true
				connected_to_ground = true
				if !grounded:
					just_landed_vel = velocity
				grounded = true
				last_gravity_direction = up_normal
				velocity = Vector3.ZERO
				current_position = collision_point + up_normal * car_definition.hover_height
				travel_direction = (travel_direction - travel_direction.dot(up_normal) * up_normal).normalized()
				floor_type = other.object_type
			if connected_to_ground:
				if !grounded:
					to_gravity = Quaternion(gravity_basis.y, up_normal.lerp(gravity_basis.y, 0.8).normalized())
					current_orientation = to_gravity * current_orientation
				gravity_orientation = to_gravity * gravity_orientation
		#Debug.record( "ground collision i guess", Time.get_ticks_usec() - tt )
	
	var was_grounded := grounded
	if neither_hit:
		connected_to_ground = false
	
	if !just_landed_vel.is_zero_approx():
		var evenness := current_transform.basis.y.dot(gravity_basis.y)
		evenness = remap(absf(evenness), 0.0, 1.0, 0.7, 0.95)
		base_speed = just_landed_vel.length() * MXGlobal.tick_delta * evenness
		air_tilt = 0
		lift_force_vel = just_landed_vel.dot(gravity_basis.y) * 10
		lift_force = maxf(lift_force_vel * MXGlobal.tick_delta, -car_definition.weight * 0.02)
		just_landed_vel = Vector3.ZERO
		if air_time < 12 and in_accel:
			landing_particles.restart()
			base_speed += car_definition.shift_boost_curve.sample_baked(kmh)
		air_time = 0
	elif grounded:
		var sideways := travel_direction.cross(gravity_basis.y).normalized()
		var flattened := gravity_basis.y.slide(sideways).normalized()
		var flattened_prev := gravity_basis_prev.y.slide(sideways).normalized()
		var angle_change := flattened.signed_angle_to(flattened_prev, sideways)
		cam_pitch_vel += angle_change * 3.0
		if ground_time > 5:
			lift_force_vel += angle_change * apparent_vel_length * 40
			if in_accel:
				lift_force_vel += -(lift_force + car_definition.weight * 0.01) * MXGlobal.tick_delta * 640
			lift_force_vel += -lift_force_vel * MXGlobal.tick_delta * 12
			lift_force_vel += -1200.0 * MXGlobal.tick_delta
		lift_force += lift_force_vel * MXGlobal.tick_delta
		lift_force = maxf(lift_force, -car_definition.weight * 0.02)
	if !collided or lift_force > 0:
		desired_position = current_position + total_movement * MXGlobal.tick_delta
		current_position = desired_position
		if grounded:
			velocity = apparent_vel_normalized * base_speed * 60
		grounded = false
	
	current_orientation = current_orientation.normalized()
	
	# rail test
	grav_point_cast.collision_mask = 3
	for point in car_definition.wall_colliders:
		var test_point_old := previous_position
		var test_point := current_orientation * point + current_position
		grav_point_cast.position = test_point_old
		grav_point_cast.target_position = grav_point_cast.to_local( test_point )
		grav_point_cast.force_update_transform()
		grav_point_cast.force_raycast_update()
		if grav_point_cast.is_colliding():
			if was_grounded:
				grounded = true
				collided = true
				velocity = Vector3.ZERO
			var collision_point := grav_point_cast.get_collision_point()
			var collision_normal := grav_point_cast.get_collision_normal().slide(gravity_basis.y).normalized()
			var depth := (test_point - collision_point).dot(collision_normal)
			if grav_point_cast.get_collider().get_collision_layer_value(1) == true:
				current_position += collision_normal * -depth
			else:
				var directness := -apparent_vel_normalized.dot(collision_normal)
				knockback_vel += (apparent_vel_length * directness * collision_normal) / (1.0 + knockback_vel.length())
				if directness > 0:
					if grounded:
						angle_vel += collision_normal.cross(gravity_basis.y) * clampf(directness * apparent_vel_length * -0.1, -12, 12)
					base_speed *= (1.0 - pow(directness, 2))
					var dmg := directness * apparent_vel_length * 0.02 + 0.25
					health -= dmg
					damage += dmg
					if directness * apparent_vel_length > 5 and !MXGlobal.currentlyRollback:
						MXGlobal.play_sound_from_node(preload("res://content/base/sound/car/wall_collision_heavy.wav"), game_car_mesh)
						if get_multiplayer_authority() == multiplayer.get_unique_id():
							for i in ceili(maxf(1.0, dmg * 2.0)):
								var spark := preload("res://content/base/effects/particles/BallSpark.tscn").instantiate()
								spark.position = collision_point
								spark.velocity = apparent_vel * randf_range(0.9, 1.05) + collision_normal * apparent_vel_length * 0.2
								spark.velocity += gravity_basis.y * apparent_vel_length * randf_range(0.0, 0.2)
								#spark.velocity += altered_normal * apparent_vel_length * randf_range(0.2, 0.5)
								spark.velocity += Vector3(randf_range(apparent_vel_length * -0.15, apparent_vel_length * 0.15), randf_range(apparent_vel_length * -0.15, apparent_vel_length * 0.15), randf_range(apparent_vel_length * -0.15, apparent_vel_length * 0.15))
								MXGlobal.add_child(spark)
				travel_direction = travel_direction.slide(collision_normal).normalized()
				if depth < 0.0:
					current_position += collision_normal * (-depth + 0.3)
	grav_point_cast.collision_mask = 1
	knockback_vel += -knockback_vel * MXGlobal.tick_delta * 4

func test_car_collision() -> void:
	var spin_time_float := 1.0 - clampf((maxf(0, spin_time - Engine.physics_ticks_per_second * 2.0)) / Engine.physics_ticks_per_second, 0.0, 1.0)
	var spinning := spin_time_float < 0.99
	var our_id:int = get_parent().playerID
	#var our_pl := get_parent() as ROPlayer
	var our_old_health := health
	for pl:ROPlayer in MXGlobal.currentStageOverseer.players:
		if pl.controlledPawn == self: continue
		var car:MXRacer = pl.controlledPawn
		var collided := false
		if current_position.distance_squared_to(car.current_position) > 4.0:
			continue
		var car_old_health := car.health
		var relative_vel := apparent_vel - car.apparent_vel
		var dir := car.previous_position - previous_position
		if dir.dot(relative_vel) <= 0:
			continue
		var car_spin_time_float := 1.0 - clampf((maxf(0, car.spin_time - Engine.physics_ticks_per_second * 2.0)) / Engine.physics_ticks_per_second, 0.0, 1.0)
		var car_spinning : bool = car_spin_time_float < 0.99
		for seg1 in car_definition.car_colliders:
			for seg2 in car.car_definition.car_colliders:
				var points : PackedVector3Array = Geometry3D.get_closest_points_between_segments(
					current_transform * seg1.p1,
					current_transform * seg1.p2,
					car.current_transform * seg2.p1,
					car.current_transform * seg2.p2
				)
				if points[0].distance_to(points[1]) < seg1.radius + seg2.radius:
					collided = true
					var normal := (car.previous_position - previous_position).normalized()
					var impulse := normal * relative_vel.dot(normal) * -0.8
					var impulse_strength : float = impulse.length()
					if !MXGlobal.currentlyRollback:
						for i in ceili(maxf(0.0, impulse_strength * 0.2)):
							var spark := preload("res://content/base/effects/particles/BallSpark.tscn").instantiate()
							spark.position = (current_position + car.current_position) * 0.5
							spark.velocity = apparent_vel * randf_range(0.9, 1.05)
							spark.velocity += gravity_basis.y * apparent_vel_length * randf_range(0.0, 0.2)
							#spark.velocity += altered_normal * apparent_vel_length * randf_range(0.2, 0.5)
							spark.velocity += Vector3(randf_range(apparent_vel_length * -0.15, apparent_vel_length * 0.15), randf_range(apparent_vel_length * -0.15, apparent_vel_length * 0.15), randf_range(apparent_vel_length * -0.15, apparent_vel_length * 0.15))
							MXGlobal.add_child(spark)
					#nobody is spinning
					if !spinning and !car_spinning:
						knockback_vel += impulse
						car.knockback_vel -= impulse
						
						health -= impulse_strength * 0.05
						car.health -= impulse_strength * 0.05
						damage += impulse_strength * 0.05
						car.damage += impulse_strength * 0.05
					#we're spinning, they aren't
					if spinning and !car_spinning:
						impulse_strength += 2.0
						
						knockback_vel += impulse
						car.knockback_vel -= impulse.normalized() * 2 * impulse_strength
						
						car.health -= impulse_strength * 0.75
						car.damage += impulse_strength * 0.75
						
					#they're spinning, we aren't
					if !spinning and car_spinning:
						impulse_strength += 2.0
						
						car.knockback_vel -= impulse
						knockback_vel += impulse.normalized() * 2 * impulse_strength
						
						health -= impulse_strength * 0.75
						damage += impulse_strength * 0.75
					
					#we're both spinning
					if spinning and car_spinning:
						knockback_vel += impulse * 0.2
						car.knockback_vel -= impulse * 0.2
					#preload("res://content/base/sound/ui/killsound_other.wav")
					#preload("res://content/base/sound/ui/killsound_own.wav")
					if car.health <= 0 and car.state == CarState.PLAY and car_old_health > 0:
						boost_recharge += calced_max_health * 0.5
						max_health_boost += car_definition.boost_health_cost * 1.2
						if MXGlobal.localPlayer and our_id == MXGlobal.localPlayer.playerID:
							MXGlobal.play_sound_for_peer(preload("res://content/base/sound/ui/killsound_own.wav"), MXGlobal.localPlayer.playerID, -5)
						else:
							MXGlobal.play_sound(preload("res://content/base/sound/ui/killsound_other.wav"), -10)
						if !MXGlobal.currentlyRollback:
							var killer_name:String= Net.peer_map[get_multiplayer_authority()].player_settings.username
							var victim_name:String = Net.peer_map[car.get_multiplayer_authority()].player_settings.username
							var badge := preload("res://content/base/ui/ko_medal.tscn").instantiate()
							MXGlobal.add_control_to_feed(badge)
							badge.set_names(killer_name, victim_name)
					
					if health <= 0 and state == CarState.PLAY and our_old_health > 0:
						car.boost_recharge += car.calced_max_health * 0.5
						car.max_health_boost += car.car_definition.boost_health_cost * 1.2
						if MXGlobal.localPlayer and pl.playerID == MXGlobal.localPlayer.playerID:
							MXGlobal.play_sound_for_peer(preload("res://content/base/sound/ui/killsound_own.wav"), MXGlobal.localPlayer.playerID, -5)
						else:
							MXGlobal.play_sound(preload("res://content/base/sound/ui/killsound_other.wav"), -10)
						if !MXGlobal.currentlyRollback:
							var killer_name:String = Net.peer_map[car.get_multiplayer_authority()].player_settings.username
							var victim_name:String = Net.peer_map[get_multiplayer_authority()].player_settings.username
							var badge := preload("res://content/base/ui/ko_medal.tscn").instantiate()
							MXGlobal.add_control_to_feed(badge)
							badge.set_names(killer_name, victim_name)
					
					var depth : float = (points[1] - points[0]).dot(normal) - (seg1.radius + seg2.radius)
					current_position += normal * depth * 0.51
					car.current_position += normal * -depth * 0.51
					break
				if collided: break
			if collided: break

func respawn_car_at_last_valid_position() -> void:
	var current_stage := MXGlobal.currentStage
	var checkpoint_respawns := current_stage.checkpoint_respawns
	var ccp := checkpoint_respawns[current_point_real]
	#var curve : Curve3D = MXGlobal.currentStage.track_path.curve
	car_visual.visible = true
	var use_basis := ccp.respawn_transform.basis.orthonormalized()
	gravity_basis = use_basis
	gravity_basis_prev = gravity_basis
	gravity_orientation = gravity_basis.get_rotation_quaternion()
	gravity_orientation_prev = gravity_orientation
	current_orientation = gravity_orientation
	previous_orientation = gravity_orientation
	previous_apparent_vel = Vector3.ZERO
	current_position = ccp.respawn_transform.origin + gravity_basis.y * 0.1
	previous_position = ccp.respawn_transform.origin + gravity_basis.y * 0.1
	current_transform = Transform3D(gravity_basis, current_position)
	previous_transform = current_transform
	apparent_vel = Vector3.ZERO
	apparent_vel_length = 0
	apparent_vel_normalized = Vector3.ZERO
	velocity = Vector3.ZERO
	base_speed = 0
	angle_vel = Vector3.ZERO
	knockback_vel = Vector3.ZERO
	cam_yaw = 0
	cam_pitch = 0
	air_tilt = 0
	ground_time = 0
	grounded = true
	current_turbo = 0
	connected_to_ground = true
	boost_time = 0
	boostpad_time = 0
	boost_state = BoostState.NONE
	health = max(calced_max_health * 0.5, health - 10)
	change_state(CarState.PLAY)

func move_broken_car() -> void:
	previous_position = current_position
	previous_orientation = current_orientation
	velocity += gravity_basis.y * -120 * MXGlobal.tick_delta
	velocity += -velocity * MXGlobal.tick_delta * 0.15
	velocity += -velocity.normalized() * MXGlobal.tick_delta * 10.0
	var desired_transform : Transform3D = Transform3D(current_orientation, current_position)
	desired_transform.origin += velocity * MXGlobal.tick_delta
	if !angle_vel.is_zero_approx():
		desired_transform.basis = desired_transform.basis.rotated(angle_vel.normalized(), angle_vel.length() * MXGlobal.tick_delta).orthonormalized()
	# MOVE AND COLLIDE BROKEN CAR
	grav_point_cast.set_collision_mask_value(2, true)
	grav_point_cast.position = current_position
	grav_point_cast.force_update_transform()
	for point in car_definition.wall_colliders:
		var global_point : Vector3 = (desired_transform * point) - Vector3(0, 0.25, 0)
		grav_point_cast.target_position = grav_point_cast.to_local(global_point)
		grav_point_cast.force_raycast_update()
		if grav_point_cast.is_colliding() and velocity.dot(grav_point_cast.get_collision_normal()) <= 0:
			var depth : float = (grav_point_cast.get_collision_point() - global_point).dot(grav_point_cast.get_collision_normal())
			var collider := grav_point_cast.get_collider() as StaticBody3D
			desired_transform.origin += grav_point_cast.get_collision_normal() * (depth + 0.005)
			force_update_transform()
			#var amount : float = velocity.dot(grav_point_cast.get_collision_normal())
			#current_transform.basis = current_transform.basis.rotated(velocity.cross(grav_point_cast.get_collision_normal()).normalized(), amount * -0.001)
			var restitution : float = 0.5
			var impulse : Vector3 = grav_point_cast.get_collision_normal() * velocity.dot(grav_point_cast.get_collision_normal()) * (-1.0 - restitution)
			velocity += impulse
			if collider.get_collision_mask_value(2):
				var dot := velocity.normalized().dot(grav_point_cast.get_collision_normal())
				dot = maxf(0, -dot)
				velocity += (gravity_basis.y * apparent_vel_length * 0.4) * dot
				velocity *= 1.0 + dot * 0.8
	grav_point_cast.set_collision_mask_value(2, false)
	if 0.0 <= MXGlobal.kmh_to_ups * 600:
		angle_vel += -angle_vel * MXGlobal.tick_delta * 6
		current_transform.basis = current_transform.basis.slerp(gravity_basis, MXGlobal.tick_delta * 6)
	
	current_position = desired_transform.origin
	current_orientation = Quaternion(desired_transform.basis)
	gravity_orientation_prev = gravity_orientation

func tick( player:ROPlayer ) -> void:
	if Engine.is_editor_hint(): return
	
	var debug_time := Time.get_ticks_usec()
	
	just_ticked = true
	calced_max_health = car_definition.max_health + max_health_boost
	current_transform = Transform3D(Basis(current_orientation), current_position)
	previous_transform = Transform3D(Basis(previous_orientation), previous_position)
	previous_apparent_vel = apparent_vel
	apparent_vel = (current_position - previous_position) * Engine.physics_ticks_per_second
	apparent_vel_length = apparent_vel.length()
	apparent_vel_normalized = apparent_vel.normalized()
	gravity_orientation = gravity_orientation.normalized()
	gravity_basis_prev = Basis(gravity_orientation_prev)
	gravity_basis = Basis(gravity_orientation)
	
	# OPTIMIZATION NOTE if you are referencing something (X.Y, X.Y.Z, etc) more than once every time
	# it's generally faster to store it in a variable
	var current_stage := MXGlobal.currentStage
	var checkpoint_respawns := current_stage.checkpoint_respawns
	
	var prev_lap := lap
	
	# first, for lap progress
	
	
	#var inner_t := Time.get_ticks_usec()
	
	var cur_cp := checkpoint_respawns[current_point]
	var next_cp_index := wrapi(current_point + 1, 0, checkpoint_respawns.size())
	var next_cp := checkpoint_respawns[next_cp_index]
	
	var in_front := next_cp.checkpoint_plane.is_point_over(current_position)
	if in_front:
		var intersect:Variant = next_cp.checkpoint_plane.intersects_segment(previous_position, current_position)
		if next_cp.required_checkpoint and intersect and intersect.distance_to(next_cp.position) < next_cp.radius:
			current_point = next_cp_index
			current_point_real = next_cp_index
			if next_cp_index == 0:
				lap += 1
		else:
			current_point = next_cp_index
	
	# handle going backwards, ignores radius completely - going backwards is easier and thus more punishing!
	
	in_front = cur_cp.checkpoint_plane.is_point_over(current_position)
	var went_back := false
	if !in_front:
		current_point = wrapi(current_point - 1, 0, checkpoint_respawns.size())
		if current_point < current_point_real:
			went_back = true
			current_point_real = current_point
		if current_point == checkpoint_respawns.size() - 1:
			went_back = true
			current_point_real = current_point
			lap -= 1
	
	# then, for respawning
	
	if !went_back:
		cur_cp = checkpoint_respawns[current_point_real]
		next_cp_index = wrapi(current_point_real + 1, 0, checkpoint_respawns.size())
		next_cp = checkpoint_respawns[next_cp_index]
		
		in_front = next_cp.checkpoint_plane.is_point_over(current_position)
		var intersect:Variant = next_cp.checkpoint_plane.intersects_segment(previous_position, current_position)
		if in_front and intersect:
			if intersect.distance_to(next_cp.position) < next_cp.radius:
				current_point_real = next_cp_index
				if next_cp_index == 0:
					lap += 1
	
	cur_cp = checkpoint_respawns[current_point]
	next_cp_index = wrapi(current_point + 1, 0, checkpoint_respawns.size())
	next_cp = checkpoint_respawns[next_cp_index]
	var p1 := cur_cp.checkpoint_plane.project(current_position)
	var p2 := next_cp.checkpoint_plane.project(current_position)
	#var p_dir := (p2 - p1).normalized()
	var p_len := (p2 - p1).length()
	var fixed_origin := Geometry3D.get_closest_point_to_segment(current_position, p1, p2) - p1
	var t := (fixed_origin / p_len).length()
	lap_progress = (current_point + t) / checkpoint_respawns.size()
	
	if lap != prev_lap:
		if !MXGlobal.currentlyRollback and get_parent().get_multiplayer_authority() == multiplayer.get_unique_id():
			if lap == 2 and prev_lap == 1 and MXGlobal.current_race_settings.laps >= 2:
				MXGlobal.play_announcer_line("boost_power")
			if lap == MXGlobal.current_race_settings.laps and prev_lap == MXGlobal.current_race_settings.laps - 1:
				MXGlobal.play_announcer_line("final_lap")
		if lap >= MXGlobal.current_race_settings.laps + 1:
			change_state(CarState.GOAL)
			if !MXGlobal.currentlyRollback and MXGlobal.currentStageOverseer.localTick == level_win_time + 90 and get_parent().get_multiplayer_authority() == multiplayer.get_unique_id():
				MXGlobal.play_announcer_line("finish")
	
	# OPTIMIZATION NOTE match statements are slower than if chains in godot
	if state == CarState.PLAY:
		do_car_driving( player.get_suitable_input() )
		
		if !current_stage.stageFalloutBounds.has_point(current_position):
			change_state(CarState.FALL)
		
		if health <= 0:
			change_state(CarState.DYING)
	elif state == CarState.WAITING:
		do_car_driving()
		if MXGlobal.currentStageOverseer.localTick == levelStartTime:
			change_state(CarState.PLAY)
	elif state == CarState.FALL:
		do_car_driving()
		if MXGlobal.currentStageOverseer.localTick - last_state_change > 60:
			respawn_car_at_last_valid_position()
	elif state == CarState.DYING:
		gravity_orientation = gravity_orientation.slerp(Quaternion.IDENTITY, MXGlobal.tick_delta * 12)
		if MXGlobal.currentStageOverseer.localTick - last_state_change > 240:
			respawn_car_at_last_valid_position()
		if MXGlobal.currentStageOverseer.localTick - last_state_change == 180 and !MXGlobal.currentlyRollback:
			var explosion := preload("res://core/effect/explosion.tscn").instantiate() as Effect
			explosion.life_time = 2000
			explosion.position = current_transform.origin
			explosion.basis = gravity_basis
			explosion.scale = Vector3.ONE * 2.0
			MXGlobal.add_child(explosion)
			car_visual.visible = false
		if MXGlobal.currentStageOverseer.localTick - last_state_change >= 180:
			return
		move_broken_car()
	elif state == CarState.GOAL:
		if health <= 0.0:
			if MXGlobal.currentStageOverseer.localTick - last_state_change == 180 and !MXGlobal.currentlyRollback:
				var explosion := preload("res://core/effect/explosion.tscn").instantiate() as Effect
				explosion.life_time = 6000
				explosion.position = current_transform.origin
				explosion.basis = gravity_basis
				explosion.scale = Vector3.ONE * 2.0
				MXGlobal.add_child(explosion)
				car_visual.visible = false
			if MXGlobal.currentStageOverseer.localTick - last_state_change > 180:
				velocity = Vector3.ZERO
				return
			move_broken_car()
		else:
			do_car_driving()
	
	Debug.record("tick", Time.get_ticks_usec() - debug_time)
	
	#var end_time := Time.get_ticks_usec()
	#print(end_time - start_time)

func save_state() -> PackedByteArray:
	car_buffer.data_array = []
	car_buffer.resize(512)
	var byteData : int = 0
	if grounded:
		byteData |= 1
	if on_booster:
		byteData |= 2
	if no_gravity_reset:
		byteData |= 4
	if connected_to_ground:
		byteData |= 8
	car_buffer.put_u8(byteData)
	car_buffer.put_u8(state)
	car_buffer.put_u8(boost_state)
	car_buffer.put_u8(steer_state)
	car_buffer.put_u8(floor_type)
	car_buffer.put_u8(lap)
	car_buffer.put_u32(last_state_change)
	car_buffer.put_u32(ground_time)
	car_buffer.put_u32(air_time)
	car_buffer.put_u32(boost_time)
	car_buffer.put_u32(boostpad_time)
	car_buffer.put_u32(spin_time)
	car_buffer.put_u32(current_point)
	car_buffer.put_u32(current_point_real)
	car_buffer.put_data( PackedFloat32Array([
		current_position.x,
		current_position.y,
		current_position.z,
		previous_position.x,
		previous_position.y,
		previous_position.z,
		apparent_vel.x,
		apparent_vel.y,
		apparent_vel.z,
		previous_apparent_vel.x,
		previous_apparent_vel.y,
		previous_apparent_vel.z,
		velocity.x,
		velocity.y,
		velocity.z,
		knockback_vel.x,
		knockback_vel.y,
		knockback_vel.z,
		angle_vel.x,
		angle_vel.y,
		angle_vel.z,
		last_ground_position.x,
		last_ground_position.y,
		last_ground_position.z,
		last_gravity_direction.x,
		last_gravity_direction.y,
		last_gravity_direction.z,
		travel_direction.x,
		travel_direction.y,
		travel_direction.z,
		current_orientation.x,
		current_orientation.y,
		current_orientation.z,
		current_orientation.w,
		previous_orientation.x,
		previous_orientation.y,
		previous_orientation.z,
		previous_orientation.w,
		gravity_orientation.x,
		gravity_orientation.y,
		gravity_orientation.z,
		gravity_orientation.w,
		gravity_orientation_prev.x,
		gravity_orientation_prev.y,
		gravity_orientation_prev.z,
		gravity_orientation_prev.w,
		health,
		max_health_boost,
		base_speed,
		lift_force_vel,
		lift_force,
		air_tilt,
		drift_dot,
		lap_progress,
		current_strafe,
		current_steering,
		current_boost_strength,
		current_turbo,
		boost_recharge,
		turn_reaction_effect,
		steer_current,
		steer_old
	]).to_byte_array())
	
	car_buffer.resize(car_buffer.get_position())
	
	return car_buffer.data_array

func load_state(inData : PackedByteArray) -> void:
	car_buffer.data_array = inData
	var byteData : int = car_buffer.get_u8()
	
	grounded = byteData & 1
	on_booster = byteData & 2
	no_gravity_reset = byteData & 4
	connected_to_ground = byteData & 8
	state = car_buffer.get_u8() as CarState
	boost_state = car_buffer.get_u8() as BoostState
	steer_state = car_buffer.get_u8() as SteerState
	floor_type = car_buffer.get_u8() as RORStageObject.object_types
	lap = car_buffer.get_u8()
	last_state_change = car_buffer.get_u32()
	ground_time = car_buffer.get_u32()
	air_time = car_buffer.get_u32()
	boost_time = car_buffer.get_u32()
	boostpad_time = car_buffer.get_u32()
	spin_time = car_buffer.get_u32()
	current_point = car_buffer.get_u32()
	current_point_real = car_buffer.get_u32()
	current_position = Vector3(car_buffer.get_float(), car_buffer.get_float(), car_buffer.get_float())
	previous_position = Vector3(car_buffer.get_float(), car_buffer.get_float(), car_buffer.get_float())
	apparent_vel = Vector3(car_buffer.get_float(), car_buffer.get_float(), car_buffer.get_float())
	previous_apparent_vel = Vector3(car_buffer.get_float(), car_buffer.get_float(), car_buffer.get_float())
	velocity = Vector3(car_buffer.get_float(), car_buffer.get_float(), car_buffer.get_float())
	knockback_vel = Vector3(car_buffer.get_float(), car_buffer.get_float(), car_buffer.get_float())
	angle_vel = Vector3(car_buffer.get_float(), car_buffer.get_float(), car_buffer.get_float())
	last_ground_position = Vector3(car_buffer.get_float(), car_buffer.get_float(), car_buffer.get_float())
	last_gravity_direction = Vector3(car_buffer.get_float(), car_buffer.get_float(), car_buffer.get_float())
	travel_direction = Vector3(car_buffer.get_float(), car_buffer.get_float(), car_buffer.get_float())
	current_orientation = Quaternion(car_buffer.get_float(), car_buffer.get_float(), car_buffer.get_float(), car_buffer.get_float())
	previous_orientation = Quaternion(car_buffer.get_float(), car_buffer.get_float(), car_buffer.get_float(), car_buffer.get_float())
	gravity_orientation = Quaternion(car_buffer.get_float(), car_buffer.get_float(), car_buffer.get_float(), car_buffer.get_float())
	gravity_orientation_prev = Quaternion(car_buffer.get_float(), car_buffer.get_float(), car_buffer.get_float(), car_buffer.get_float())
	health = car_buffer.get_float()
	max_health_boost = car_buffer.get_float()
	base_speed = car_buffer.get_float()
	lift_force_vel = car_buffer.get_float()
	lift_force = car_buffer.get_float()
	air_tilt = car_buffer.get_float()
	drift_dot = car_buffer.get_float()
	lap_progress = car_buffer.get_float()
	current_strafe = car_buffer.get_float()
	current_steering = car_buffer.get_float()
	current_boost_strength = car_buffer.get_float()
	current_turbo = car_buffer.get_float()
	boost_recharge = car_buffer.get_float()
	turn_reaction_effect = car_buffer.get_float()
	steer_current = car_buffer.get_float()
	steer_old = car_buffer.get_float()

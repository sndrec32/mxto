@tool
class_name RORStageObject extends StaticBody3D

var stageMesh : MeshInstance3D
var stageCol : CollisionShape3D
var mesh_data: MeshDataTool

enum object_types {
	NONE,
	PITSTOP,
	ICE,
	DIRT,
	RAIL
}

@export var object_type : object_types = object_types.NONE
@export var enableCollision : bool = true
@export var enableTrigger : bool = false
@export var enableBackfaceCollisions : bool = false:
	set(new_bool):
		enableBackfaceCollisions = new_bool
		if has_node("ObjCol") and stageCol:
			stageCol.shape.backface_collision = new_bool

var vertex_normals : Array[PackedVector3Array] = []
var vertex_positions : Array[PackedVector3Array] = []
var road_info : Array[PackedVector3Array] = []

func _ready() -> void:
	if Engine.is_editor_hint():
		get_parent().set_editable_instance(self, true)
		return
	var mesh : Mesh = get_child(0).mesh
	mesh_data = MeshDataTool.new()
	mesh_data.create_from_surface(mesh, 0)
	var collider : CollisionShape3D = CollisionShape3D.new()
	var new_shape : ConcavePolygonShape3D = ConcavePolygonShape3D.new()
	new_shape.set_faces(mesh.get_faces())
	collider.shape = new_shape
	add_child(collider)
	obj_ready()
	add_to_group("StageObjects")
	for i in mesh_data.get_face_count():
		vertex_normals.append(get_vertex_normals_at_face_index(i))
		vertex_positions.append(get_vertex_positions_at_face_index(i))
		road_info.append(get_road_collision_info(i))
	match object_type:
		object_types.NONE:
			set_collision_mask_value(2, false)
			set_collision_layer_value(2, false)
			set_collision_mask_value(1, true)
			set_collision_layer_value(1, true)
		object_types.RAIL:
			set_collision_mask_value(1, false)
			set_collision_layer_value(1, false)
			set_collision_mask_value(2, true)
			set_collision_layer_value(2, true)

func _post_stage_loaded() -> void:
	pass

func obj_ready() -> void:
	pass

func _process(delta : float) -> void:
	obj_process(delta)
	if Engine.is_editor_hint():
		return
	if !has_node("ObjMesh"):
		return

func obj_process(_delta : float) -> void:
	pass

func evaluate(command:String, variable_names := [], variable_values := []) -> void:
	var expression := Expression.new()
	var error := expression.parse(command, variable_names)
	if error != OK:
		push_error(expression.get_error_text())
		return

	var result:Variant = expression.execute(variable_values, self)

	if not expression.has_execute_failed():
		print(str(result))

func on_trigger( _inBall: DefaultBall, _inFrac : float, _backface : bool) -> void:
	pass

func get_vertex_normals_at_face_index(index: int) -> PackedVector3Array:
	return PackedVector3Array([
		global_transform.basis * mesh_data.get_vertex_normal( mesh_data.get_face_vertex( index, 0 ) ),
		global_transform.basis * mesh_data.get_vertex_normal( mesh_data.get_face_vertex( index, 1 ) ),
		global_transform.basis * mesh_data.get_vertex_normal( mesh_data.get_face_vertex( index, 2 ) ),
	])

func get_vertex_positions_at_face_index(index: int) -> PackedVector3Array:
	return PackedVector3Array([
		global_transform * mesh_data.get_vertex( mesh_data.get_face_vertex( index, 0 ) ),
		global_transform * mesh_data.get_vertex( mesh_data.get_face_vertex( index, 1 ) ),
		global_transform * mesh_data.get_vertex( mesh_data.get_face_vertex( index, 2 ) ),
	])

func get_road_collision_info(index: int) -> PackedVector3Array:
	var fv0 := mesh_data.get_face_vertex( index, 0 )
	var fv1 := mesh_data.get_face_vertex( index, 1 )
	var fv2 := mesh_data.get_face_vertex( index, 2 )
	return PackedVector3Array([
		global_transform * mesh_data.get_vertex( fv0 ),
		global_transform * mesh_data.get_vertex( fv1 ),
		global_transform * mesh_data.get_vertex( fv2 ),
		global_transform.basis * (mesh_data.get_vertex_normal( fv0 )),
		global_transform.basis * (mesh_data.get_vertex_normal( fv1 )),
		global_transform.basis * (mesh_data.get_vertex_normal( fv2 )),
	])

var saveBuffer := StreamPeerBufferExtension.new()
func save_state() -> PackedByteArray:
	saveBuffer.data_array = []
	var byteData := 0
	if enableCollision:
		byteData |= 1
	if enableTrigger:
		byteData |= 2
	saveBuffer.put_u8( byteData )
	return saveBuffer.data_array

func load_state(inData : PackedByteArray) -> void:
	saveBuffer.data_array = inData
	var byteData := saveBuffer.get_u8()
	enableCollision = byteData & 1
	enableTrigger = byteData & 2
